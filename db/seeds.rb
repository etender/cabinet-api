# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

require './db/seeds/company_seed'
require './db/seeds/users_seed'