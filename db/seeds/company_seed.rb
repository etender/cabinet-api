module CompanySeed
  company_name  = Rails.application.credentials.dig(Rails.env.to_sym, :company, :name) || 'Гартендер'

  unless Company.where(name: company_name).exists?
    Company.create_with_tariff(
      name: company_name,
      tariff: Rails.application.credentials.dig(Rails.env.to_sym, :company, :name) || 'full'
    )
  end
end
