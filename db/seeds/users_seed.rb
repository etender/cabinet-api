module UsersSeed

  # Create admin
  unless User.where(email: User.admin_email).exists?
    company = Company.root
    user = company.users.new(
      email: User.admin_email,
      password: Rails.application.credentials.dig(Rails.env.to_sym, :admin, :password) || '123456',
      name: 'Admin',
      time_zone: 'Europe/Moscow',
      admin: true,
      developer: true
    )
    # Skip confirmation because DeviseMailer#confirmation_instructions (app/mailers/devise_mailer.rb) change password
    user.skip_confirmation_notification! if Rails.env.development?
    user.save!
    user.confirm
  end
end
