require 'sidekiq/web'
require 'sidekiq-scheduler/web'

Rails.application.routes.draw do
  authenticate :user, ->(user) { user.developer? } do
    mount Sidekiq::Web => '/admin/workers'
  end

  devise_for :user,
             skip: %i[session confirmations registrations passwords invitations]

  devise_scope :user do
    namespace :user do
      resource :sessions, only: [], path: '' do
        post :create, path: :sign_in
        delete :destroy, path: :sign_out, as: :destroy
      end

      resource :registrations, only: %i[show create update] do
        post :prolong
      end

      resource :password, only: %i[create update edit]

      resource :confirmation, only: %i[show create destroy] do
        put :update
      end

      resource :invitation, only: %i[show create update] do
        get :show, as: :accept
      end

      resources :invitations, only: %i[index destroy]
    end
  end

  resources :users, only: %i[index destroy]

  resources :filters, only: %i[index show create update destroy] do
    post :build, path: :entity, on: :collection
    get :copy, path: :copy
  end

  resources :favorites, only: %i[index show create destroy] do
    scope module: :favorites do
      resources :user, only: %i[update]
      resources :state, only: %i[update]

      collection do
        resources :exports, only: %i[create]
      end
    end
  end

  resources :tenders, only: %i[index show] do
    scope module: :tender, shallow: true do
      resources :comments, only: %i[index create show update destroy]

      collection do
        resources :exports, only: %i[create]
      end
    end
  end

  resources :suggestions, only: %i[index]

  resources :okpds, only: %i[index]

  resources :categories, only: %i[index]

  resources :regions, only: %i[index]

  resources :organizations, only: %i[index]

  resources :time_zones, only: %i[index]

  resources :short_links, only: %i[create show]

  get :doc_constructor, to: 'other_services#doc_constructor'

  def draw_analytics_customer
    namespace :customers do
      resources :purchases, only: %i[index] do
        scope module: :purchases do
          collection do
            resources :exports, only: %i[create]
          end
        end
      end

      resources :tenders, only: %i[index]

      resources :suppliers, only: %i[index] do
        scope module: :suppliers do
          collection do
            resources :exports, only: %i[create]
          end
        end
      end

      resources :check_results, only: %i[index] do
        scope module: :check_results do
          collection do
            resources :exports, only: %i[create]
          end
        end
      end
    end
  end

  namespace :analytics do
    resources :filters, only: %i[index] do
      post '/:entity', action: :build, on: :collection, as: :build
      # post :build, path: :entity, on: :collection
    end

    draw_analytics_customer

    namespace :suppliers do
      resources :purchases, only: %i[index] do
        scope module: :purchases do
          collection do
            resources :exports, only: %i[create]
          end
        end
      end

      resources :supplier_description, only: %i[index] do
        scope module: :supplier_description do
          collection do
            resources :reports, only: %i[create show]
          end
        end
      end

      resources :tenders, only: %i[index]
    end
  end

  scope module: :api do
    namespace :private do
      resources :users, only: %i[index]
      resources :sessions, only: %i[create]
    end

    namespace :shared do
      resources :tenders, only: %i[index show] do
        collection do
          resources :exports, only: %i[create]
        end
      end

      resources :suggestions, only: %i[index]

      resources :okpds, only: %i[index]

      resources :categories, only: %i[index]

      resources :regions, only: %i[index]

      resources :organizations, only: %i[index]

      namespace :analytics do
        resources :filters, only: %i[index] do
          post :build, path: :entity, on: :collection
        end

        draw_analytics_customer
      end
    end
  end

  root 'application#index'
end
