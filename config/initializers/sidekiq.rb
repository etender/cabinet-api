require 'sidekiq/recurring_job'

url = Rails.application.credentials.dig Rails.env.to_sym, :redis, :host
url = '127.0.0.1:6379/3' if url.blank?
options = { url: "redis://#{url}" }

Sidekiq.configure_client { |config| config.redis = options }
Sidekiq.configure_server { |config| config.redis = options }