require 'elasticsearch/rails/instrumentation'

ELASTICSEARCH_CONFIG = YAML.load(ERB.new(File.read("#{Rails.root}/config/elasticsearch.yml")).result)[Rails.env].symbolize_keys
ELASTICSEARCH_CONFIG[:log] = false if ENV['ELASTICSEARCH_LOG_DISABLED'].present?
Elasticsearch::Model.client = Elasticsearch::Client.new ELASTICSEARCH_CONFIG