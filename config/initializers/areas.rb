AREAS_NAME = {}
AREAS_URL = {}

AREAS = YAML.load_file(File.join(Rails.root, 'config', 'areas.yml')).flatten
AREAS.each do |section|
  section['data'].each do |area|
    AREAS_NAME[area['id']] = area['name']
    AREAS_URL[area['id']] = area['href']
  end
end

FEDERAL_AREAS = AREAS[0]['data'][0]['included'].map { |area| area['id'] }.push('etprf_ru')
