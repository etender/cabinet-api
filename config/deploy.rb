# config valid only for current version of Capistrano
lock '~> 3.10'

set :application, 'cabinet-api'
set :repo_url, 'git@bitbucket.org:etender/cabinet-api.git'

# Rbenv settings
set :rbenv_type, :user
set :rbenv_ruby, '2.6.0'
set :rbenv_map_bins, %w[rake gem bundle ruby rails sidekiq sidekiqctl puma pumactl]
set :rbenv_roles, %w[app web]
set :rbenv_path, '/home/deployer/.rbenv'

# Sidekiq settings
set :sidekiq_config, File.join(current_path, 'config', 'sidekiq.yml')
set :sidekiq_processes, 4
set :sidekiq_user, 'deployer'
set :sidekiq_monit_group, fetch(:application)

# Client settings
set :client_path, '/var/www/cabinet-web'
set :client_application, 'cabinet-web'
set :client_config_name, "#{fetch(:client_application)}_#{fetch(:stage)}"

# Logrotate settings
set :logrotate_role, %i[app web]
set :logrotate_interval, 'hourly'
set :logrotate_logs_keep, 5

set :puma_user, 'deployer'
set :puma_workers, 4
set :puma_threads, [5, 5]
set :puma_preload_app, true

# Maintenance settings
set :maintenance_template_path, File.join(current_path, 'deploy', 'templates', 'maintenance.html')
set :maintenance_path, File.join(fetch(:maintenance_dirname), "#{fetch(:maintenance_basename)}.html")

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
set :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml", "config/secrets.yml"
append :linked_files, 'config/master.key'

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"
append :linked_dirs, '.bundle', 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'tmp/reports', 'public/system'

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5
