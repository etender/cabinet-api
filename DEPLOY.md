# Deploy

## Prepare to capistrano

1. Update packages

        sudo apt-get update
        
2. Create capistrano deployer user

        sudo adduser deployer
        su - deployer

3. Add deployer authorization under deploy   user. Replace **cabinet publick key** with content ~/.ssh/cabinet/id_rsa.pub

        mkdir .ssh
        echo "cabinet publick key" >> .ssh/authorized_keys
        chmod 700 .ssh
        chmod 600 .ssh/authorized_keys

    logout deployer

        sudo passwd -l deployer
        sudo visudo -f /etc/sudoers.d/91-deploy-user

    insert text into file

        deployer ALL=(ALL) NOPASSWD:ALL

## Prepare base to deploy

1. Install requirement packages

        sudo apt-get -y install build-essential git libcurl4-openssl-dev libffi-dev libreadline-dev libssl-dev libxml2-dev libxslt1-dev zlib1g-dev

2. Install requirement packages

        curl -sL https://deb.nodesource.com/setup_13.x | sudo -E bash -
        sudo apt-get -y install nodejs

## Prepare parser or service to deploy

1. Install requirement packages

        sudo apt-get -y install monit

2. Remove comment from lines **set httpd**    

        sudo nano  /etc/monit/monitrc
        sudo service monit restart

## Prepare web to deploy 

2. Install requirement packages (yarn to precompile assets)

        curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
        echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
        sudo apt-get update
        
        sudo apt-get -y install nginx yarn

3. Generate strong key for nginx

        sudo mkdir /etc/nginx/ssl
        sudo openssl dhparam -out /etc/nginx/ssl/dhparam.pem 2048