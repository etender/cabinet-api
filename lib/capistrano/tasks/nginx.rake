namespace :nginx do
  %i[stop start restart reload force-reload].each do |action|
    desc "#{action.to_s.capitalize} nginx"
    task action do
      on roles :app, :web do
        sudo :service, 'nginx', action.to_s
      end
    end
  end

  desc 'Setup nginx ssl certs'
  task :setup_ssl do
    def sudo_upload!(from, to)
      filename = File.basename(to)
      to_dir = File.dirname(to)
      tmp_file = "#{fetch(:tmp_dir)}/#{filename}"
      upload! from, tmp_file
      sudo :mv, tmp_file, to_dir
    end

    def file_exists?(path)
      test "[ -e #{path} ]"
    end

    next unless fetch(:nginx_use_ssl)
    on roles :web do
      nginx_ssl_certificate = fetch(:nginx_ssl_certificate)
      nginx_ssl_certificate_key = fetch(:nginx_ssl_certificate_key)
      next if file_exists?(nginx_ssl_certificate) && file_exists?(nginx_ssl_certificate_key)

      sudo_upload! fetch(:nginx_ssl_cert_local_path), nginx_ssl_certificate
      sudo_upload! fetch(:nginx_ssl_cert_key_local_path), nginx_ssl_certificate_key

      sudo :chown, 'root:root', nginx_ssl_certificate
      sudo :chown, 'root:root', nginx_ssl_certificate_key
    end
  end
end