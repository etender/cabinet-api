desc 'Server setup tasks'
task :setup do
  invoke 'safe_deploy_to:ensure'

  on roles :app, :web do
    invoke 'nginx:setup_ssl'
    invoke 'puma:nginx_config'
  end

  invoke 'sidekiq:monit:config'

  invoke 'logrotate:config'

  invoke 'credentials:setup'
end