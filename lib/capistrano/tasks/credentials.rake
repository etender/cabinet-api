namespace :credentials do
  desc 'Setup credentials master key'
  task :setup do
    on roles :all do
      path = shared_path.join('config', 'master.key')
      unless test "[ -e #{path} ]"
        upload! './config/master.key', path
      end
    end
  end
end