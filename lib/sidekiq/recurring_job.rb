module Sidekiq
  class RecurringJob
    include Sidekiq::Worker

    sidekiq_options queue: 'recurring'

    def perform(job, options = {})
      args = options.symbolize_keys
      if args.present?
        job.constantize.perform_later args
      else
        job.constantize.perform_later
      end
    end
  end
end
