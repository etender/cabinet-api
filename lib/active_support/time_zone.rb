module ActiveSupport
  class TimeZone
    def to_s
      translated_name = I18n.t tzinfo.name, scope: :time_zones
      "(GMT#{formatted_offset}) #{translated_name}"
    end
  end
end