require 'tasks/storage/helpers'

include Tasks::Storage::Helpers

namespace :storage do
  desc 'Setup new storage'
  task setup: [
    :environment,
    'db:seed',
    'storage:category:import',
    'storage:okpd:import',
    'storage:region:import']

  desc 'Restore from prev version'
  task :restore, [:folder] => [
    :environment,
    'company:restore',
    'user:restore',
    'filter:restore',
    'tender:restore']
end
