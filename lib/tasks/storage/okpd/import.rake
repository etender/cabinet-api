namespace :storage do
  namespace :okpd do
    desc 'Import okpds from ftp://ftp.zakupki.gov.ru/fcs_nsi/nsiOKPD2'
    task import: :environment do
      OkpdImportService.perform
    end
  end
end
