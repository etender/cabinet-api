namespace :storage do
  namespace :tender do
    desc 'Restore tenders from prev version'
    task :restore, [:folder] => :environment do |_, args|
      folder = File.join args[:folder], 'tenders'

      extract_all(folder) do |file_name|
        Tender.restore YAML.load_file file_name
      end
    end
  end
end
