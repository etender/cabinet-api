namespace :storage do
  namespace :filter do
    desc 'Restore filters from prev version'
    task :restore, [:folder] => :environment do |_, args|
      folder = File.join args[:folder], 'filters'

      extract_all(folder) do |file_name|
        Filter.restore YAML.load_file file_name
      end
    end
  end
end
