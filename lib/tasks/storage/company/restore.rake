namespace :storage do
  namespace :company do
    desc 'Restore companies from prev version'
    task :restore, [:folder] => :environment do |_, args|
      folder = File.join args[:folder], 'companies'

      extract_all(folder) do |file_name|
        Company.restore YAML.load_file file_name
      end
    end
  end
end