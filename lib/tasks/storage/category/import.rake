namespace :storage do
  namespace :category do
    desc 'Import regions from fixtures'
    task import: :environment do
      CategoryImportService.perform
    end
  end
end
