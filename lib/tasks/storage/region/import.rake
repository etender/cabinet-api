namespace :storage do
  namespace :region do
    desc 'Import regions from fixtures'
    task import: :environment do
      RegionImportService.perform
    end
  end
end
