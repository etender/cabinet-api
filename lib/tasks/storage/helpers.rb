module Tasks
  module Storage
    module Helpers
      # Extract all files in folder
      def extract_all(folder, &block)
        files = Dir["#{folder}/*.tar.bz2"]
        count = files.count
        files.each_with_index do |file_name, index|
          puts "[#{index + 1}/#{count}] Import file [#{file_name}]"

          extract file_name, &block
        end
      end

      # Extract exported data
      def extract(file_name)
        extract_file_name = file_name.gsub('.tar.bz2', '.yml')
        `cd #{File.dirname file_name}; tar -jxf #{File.basename file_name}`

        yield extract_file_name

        File.delete file_name
      ensure
        File.delete extract_file_name if File.exists? extract_file_name
      end
    end
  end
end
