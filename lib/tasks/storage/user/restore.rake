namespace :storage do
  namespace :user do
    desc 'Restore users from prev version'
    task :restore, [:folder] => :environment do |_, args|
      folder = File.join args[:folder], 'users'

      extract_all(folder) do |file_name|
        User.restore YAML.load_file file_name
      end
    end
  end
end
