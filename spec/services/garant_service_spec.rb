require 'rails_helper'

RSpec.describe GarantService, type: :controller do

  subject { GarantService }

  describe '.sign_in' do
    let!(:company) { create :company, name: 'cabinet' }
    let(:response) { json_fixture 'services/garant_service/success.json' }
    let(:mailer) { double }

    before do
      allow(subject).to receive(:request).and_return response
      allow(mailer).to receive :deliver_later
      allow(Devise::Mailer).to receive(:confirmation_instructions).and_return(mailer)
    end

    context 'success response' do
      it 'return user' do
        user = subject.sign_in('123')[:user]
        expect(user.id).to eq User.first.id
        expect(user.email).to eq "gsc_d033e22ae348aeb5660fc2140aec35850c4da997@cabinet.ru"
        expect(user.company_id).to eq company.id
      end

      it "don't send confirmation_email" do
        subject.sign_in('123')
        expect(mailer).to_not have_received :deliver_later
      end
    end

    context 'empty host' do
      before { response['client']['host'] = nil }

      it 'return :not_acceptable' do
        expect(subject.sign_in('123')[:error]).to eq :not_acceptable
      end
    end

    context 'another user already exists' do
      let!(:another_company) { create :company, name: 'another_cabinet' }
      before { create :user, email: response['client']['username'], company: another_company }

      it 'create new user in company' do
        expect(subject.sign_in('123')[:user].company_id).to eq company.id
      end
    end

    context "company don't exists"  do
      before { response['client']['host'] = 'myhost' }

      it 'create new company' do
        user = subject.sign_in('123')[:user]
        expect(user.company.name).to eq response['client']['host']
      end
    end
  end
end
