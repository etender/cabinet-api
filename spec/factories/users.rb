FactoryBot.define do
  factory :user do
    sequence(:name) { |index| "John#{index}" }
    password  { '123456' }
    password_confirmation { '123456' }
    email { "#{name}@test.ru".downcase }

    before(:create) do |user|
      user.company = create :company
      user.skip_confirmation!
    end
  end
end