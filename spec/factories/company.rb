FactoryBot.define do
  factory :company do
    sequence(:name) { |index| "Company #{index}" }

    after(:create) do |company|
      company.change_tariff :full
    end
  end
end