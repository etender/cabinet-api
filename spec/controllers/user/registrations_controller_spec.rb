require 'rails_helper'

RSpec.describe User::RegistrationsController, type: :controller do
  describe 'POST #update' do
    let(:params) { { user: { current_password: user.password, password: 'qwerty', password_confirmation: 'qwerty' } } }

    context 'authenticated user' do
      include_context 'authenticate as user'

      it 'change password' do
        post :update, body: params.to_json

        expect(response).to have_http_status(:created)
        expect(response.body).to eq(serialize user)
      end
    end

    context 'foreign user' do
      let(:user) { create :user, foreign: true }

      it 'return unauthorized' do
        post :update, body: params.to_json

        expect(response).to have_http_status(:unauthorized)
      end
    end
  end
end
