require 'rails_helper'

RSpec.describe User::SessionsController, type: :controller do
  describe 'POST #create' do
    context 'with empty email and password' do
      it 'return unauthorized' do
        post :create

        expect(response).to have_http_status(:unauthorized)
      end
    end

    context 'with email and password' do
      let(:user) { create :user, last_sign_in_at: Time.now }
      let(:params) { { user: { email: user.email, password: user.password } } }

      it 'return success' do
        post :create, body: params.to_json

        expect(response).to have_http_status(:success)
        expect(response.body).to eq(serialize user)
      end
    end

    context 'foreign user' do
      let(:user) { create :user, foreign: true }
      let(:params) { { user: { email: user.email, password: user.password } } }

      it 'return unauthorized' do
        post :create, body: params.to_json

        expect(response).to have_http_status(:unauthorized)
      end
    end
  end
end
