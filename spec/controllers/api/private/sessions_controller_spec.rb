require 'rails_helper'

RSpec.describe Api::Private::SessionsController, type: :controller do
  describe '#index' do
    let(:user) { create :user }
    let(:params) { { email: user.email } }

    context 'when client is authenticated' do
      include_context '[Api::Private] authenticated client'

      it 'search and return user by email' do
        post :create, body: params.to_json, xhr: true

        expect(response).to have_http_status(:ok)
        expect(response.body).to eq(serialize user.sessions.last, serializer: Api::Private::SessionSerializer)
      end
    end

    context "when client isn't authenticated" do
      it 'return forbidden' do
        post :create, body: params.to_json, xhr: true

        expect(response).to have_http_status(:forbidden)
      end
    end
  end
end
