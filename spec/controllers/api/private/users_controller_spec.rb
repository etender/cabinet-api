require 'rails_helper'

RSpec.describe Api::Private::UsersController, type: :controller do
  describe '#index' do
    let(:user) { create :user }
    let(:params) { { email: user.email } }

    context 'when client is authenticated' do
      include_context '[Api::Private] authenticated client'

      it 'search and return user by email' do
        get :index, body: params.to_json, xhr: true

        expect(response).to have_http_status(:ok)
        expect(response.body).to eq(serialize user, serializer: Api::Private::UserSerializer)
      end
    end

    context "when client isn't authenticated" do
      it 'return forbidden' do
        get :index, body: params.to_json, xhr: true

        expect(response).to have_http_status(:forbidden)
      end
    end
  end
end
