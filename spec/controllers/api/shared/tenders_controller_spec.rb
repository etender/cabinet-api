require 'rails_helper'

RSpec.describe Api::Shared::TendersController, type: :controller do
  describe 'GET #index' do
    before { allow(Tender).to receive(:with_elastic_search).and_return [[], 0] }

    context 'when client is authenticated' do
      include_context '[Api::Shared] authenticated client'

      it 'search and return user by email' do
        get :index

        expect(response).to have_http_status(:ok)
      end
    end

    context "when client isn't authenticated" do
      it 'return forbidden' do
        get :index

        expect(response).to have_http_status(:forbidden)
      end
    end
  end
end
