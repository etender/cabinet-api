require 'rails_helper'

RSpec.describe Api::Shared::CategoriesController, type: :controller do
  context "when client isn't authenticated" do
    it 'return forbidden' do
      get :index

      expect(response).to have_http_status(:forbidden)
    end
  end
end
