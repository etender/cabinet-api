class CompanyMailerPreview < ActionMailer::Preview
  def tariff_extend
    user = User.find_by email: 'admin@dev.gartender.ru'
    CompanyMailer.tariff_extend user.company.id.to_s, user.email
  end
end