module Macros
  module Controller
    def serialize(model, options = {})
      ActiveModelSerializers::SerializableResource.new(model, options).to_json
    end
  end
end