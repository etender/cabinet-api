
RSpec.configure do |config|
  config.include Macros::Controller, type: :controller

  config.before(:each, type: :controller) do
    request.env["HTTP_ACCEPT"] = 'application/json'
    request.env["CONTENT_TYPE"] = 'application/json'
  end
end