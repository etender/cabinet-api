RSpec.configure do |config|
  config.include Devise::Test::ControllerHelpers, type: :controller

  config.before(:each, type: :controller) do |config|
    @request.env['devise.mapping'] = Devise.mappings[:user]
  end
end