module FixturesHelper
  def json_fixture(path)
    JSON.parse file_fixture(path).read
  end
end

RSpec.configure do |config|
  config.include FixturesHelper
end