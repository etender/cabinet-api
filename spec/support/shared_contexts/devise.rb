RSpec.shared_context 'authenticate as user' do
  let(:user) { create :user }
  before { sign_in user }
end