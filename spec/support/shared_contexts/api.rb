RSpec.shared_context '[Api::Private] authenticated client' do
  before { allow(controller).to receive(:authenticate_by_ip!) }
end

RSpec.shared_context '[Api::Shared] authenticated client' do
  before { allow(controller).to receive(:authenticate_referer!) }
end