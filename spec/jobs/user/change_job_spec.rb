require 'rails_helper'

RSpec.describe User::ChangeJob, type: :jobs do
  describe '#perform' do
    let!(:user) { create :user }
    let(:options) { { name: user.email, phone: 'phone', name: 'first_name', surname: 'last_name', patronymic: 'middle_name' } }

    it 'change user' do
      expect { subject.perform options }
        .to change { User.first.as_document.slice(*options.keys).deep_symbolize_keys }
        .to(options)
    end
  end
end