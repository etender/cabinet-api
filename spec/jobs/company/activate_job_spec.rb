require 'rails_helper'

RSpec.describe Company::ActivateJob, type: :jobs do
  describe '#perform' do
    let!(:company) { create :company, active: false }
    let(:options) { { name: company.name, email: 'test@test.ru' } }
    let(:mailer) { double }

    before do
      allow(mailer).to receive :deliver_later
      allow(CompanyMailer).to receive(:tariff_changed).and_return(mailer)
      allow(Company).to receive(:find_by).with(options.slice :name).and_return company
    end

    it 'change company active state' do
      expect { subject.perform options }.to change { Company.first.active }.from(false).to(true)
    end

    it 'send tariff changed email' do
      subject.perform options
      expect(CompanyMailer).to have_received(:tariff_changed).with(company.id.to_s, options[:email])
    end
  end
end
