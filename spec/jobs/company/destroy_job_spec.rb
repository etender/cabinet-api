require 'rails_helper'

RSpec.describe Company::DestroyJob, type: :jobs do
  describe '#perform' do
    let!(:company) { create :company }

    it 'remove company' do
      expect { subject.perform name: company.name }.to change { Company.count }.from(1).to(0)
    end
  end
end
