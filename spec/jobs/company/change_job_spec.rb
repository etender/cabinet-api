require 'rails_helper'

RSpec.describe Company::ChangeJob, type: :jobs do
  describe '#perform' do
    let!(:company) { create :company }
    let(:options) { { name: company.name, tariff: 'trio', email: 'test@test.ru' } }
    let(:mailer) { double }

    before do
      allow(mailer).to receive :deliver_later
      allow(CompanyMailer).to receive(:tariff_changed).and_return(mailer)
      allow(Company).to receive(:find_by).with(options.slice :name).and_return company
    end

    it 'change tariff company' do
      expect(company).to receive(:change_tariff).with('trio')
      subject.perform options
    end

    it 'send tariff changed email' do
      subject.perform options
      expect(CompanyMailer).to have_received(:tariff_changed).with(company.id.to_s, options[:email])
    end

    context 'when company deactivated' do
      before { company.set active: false }

      it "change tariff company" do
        expect(company).to receive(:change_tariff)
        subject.perform options
      end

      it "doesn't send tariff changed email" do
        subject.perform options
        expect(CompanyMailer).to_not have_received(:tariff_changed)
      end
    end
  end
end
