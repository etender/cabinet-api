require 'rails_helper'

RSpec.describe Company::DeactivateJob, type: :jobs do
  describe '#perform' do
    let!(:company) { create :company }
    let(:options) { { name: company.name, email: 'test@test.ru' } }

    before do
      allow(Company).to receive(:find_by).with(options.slice :name).and_return company
    end

    it 'change company active state' do
      expect { subject.perform options }.to change { Company.first.active }.from(true).to(false)
    end
  end
end
