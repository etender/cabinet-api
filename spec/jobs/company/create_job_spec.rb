require 'rails_helper'

RSpec.describe Company::CreateJob, type: :jobs do
  describe '#perform' do
    let(:options) { {
      user: { email: 'test@test.ru', phone: 'phone', name: 'name', surname: 'last_name', patronymic: 'patronymic'},
      company: { name: 'my company', tariff: 'base' }
    } }
    let(:company_attributes) { [:name, :tariff] }
    let(:user_attributes) { [:email, :surname, :patronymic, :phone] }

    it 'create company' do
      expect { subject.perform options }
        .to change { Company.first&.as_document&.slice(*options[:company].keys)&.deep_symbolize_keys }
        .from(nil)
        .to(options[:company])
    end

    it 'create user' do
      expect { subject.perform options }
        .to change { User.first&.as_document&.slice(*options[:user].keys)&.deep_symbolize_keys }
        .from(nil)
        .to(options[:user])
    end
  end
end
