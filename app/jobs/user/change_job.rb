class User
  class ChangeJob < ApplicationJob
    queue_as :company

    def perform(options)
      User.where(options.slice :email).set options.except(:email)
    end
  end
end