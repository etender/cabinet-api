class Filter
  module Changes
    class SendJob < ApplicationJob
      queue_as :filter

      include CompanyDistributableJob

      def perform_by_entity(id)
        FilterMailer.changes_report(id).deliver
      end

      def company_entities(company_id)
        company_filters_rebuild company_id

        User.where(company_id: company_id).with_tenders_new_subscription.pluck :id
      end

      def company_filters_rebuild(company_id)
        Filter.where(company_id: company_id).each{ |filter| filter.rebuild }
      end
    end
  end
end