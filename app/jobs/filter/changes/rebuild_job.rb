class Filter
  module Changes
    class RebuildJob < ApplicationJob
      queue_as :filter

      include CompanyDistributableJob

      def perform_by_entity(id)
        Filter.where(id: id).first&.rebuild
      end

      def company_entities(company_id)
        Filter.where(company_id: company_id).pluck :id
      end
    end
  end
end