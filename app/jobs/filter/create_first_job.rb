class Filter
  class CreateFirstJob < ApplicationJob
    queue_as :filter

    include CompanyDistributableJob

    def perform_by_entity(id)
      FilterMailer.create_first(id).deliver
    end

    def company_entities(company_id)
      User.where(company_id: company_id).where(filters_quantity: nil).pluck :id
    end
  end
end