class ShortLink
  class ClearJob < ApplicationJob
    queue_as :short_links

    def perform
      ShortLink.outdated.delete_all
    end
  end
end