module CompanyDistributableJob
  extend ActiveSupport::Concern

  def perform(options = {})
    id = options[:id]
    return perform_by_entity id if id.present?

    company_id = options[:company_id]
    return perform_company_entities company_id if company_id.present?

    perform_companies
  end

  def perform_companies
    Company.active.pluck(:id).each { |id| self.class.perform_later company_id: id.to_s }
  end

  def perform_company_entities(company_id)
    company_entities(company_id).each { |id| self.class.perform_later id: id.to_s }
  end
end