class Tender
  module Status
    class RebuildJob < ApplicationJob
      queue_as :tender

      def perform
        Tender.where(status: :submission_app).lte(end_at: Time.now.utc).set status: :completed
      end
    end
  end
end