class Tender
  module Changes
    class SendJob < ApplicationJob
      queue_as :tender

      include CompanyDistributableJob

      def perform_by_entity(id)
        TenderMailer.changes_report(id).deliver
      end

      def company_entities(company_id)
        User.where(company_id: company_id).with_tenders_change_subscription.pluck :id
      end
    end
  end
end
