class Tender
  module Changes
    class RebuildJob < ApplicationJob
      queue_as :tender

      include CompanyDistributableJob

      def perform_by_entity(id)
        Tender.where(id: id).first&.rebuild_changes
      end

      def company_entities(company_id)
        Tender.where(company_id: company_id).pluck :id
      end
    end
  end
end