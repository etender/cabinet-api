class Company
  class ValidateJob < ApplicationJob
    queue_as :company

    def perform
      Company.active.expired.set active: false
    end
  end
end