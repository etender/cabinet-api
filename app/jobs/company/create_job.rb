class Company
  class CreateJob < ApplicationJob
    queue_as :company

    def perform(options)
      company = Company.create_with_tariff options[:company]
      password = Devise.friendly_token.first(8)
      company.users.create! options[:user].merge(admin: true, password: password, password_confirmation: password)
    end
  end
end