class Company
  class TrackJob < ApplicationJob
    queue_as :company

    def perform
      Company.active.each do |company|
        options = {
          host: company.name,
          tenders_count: company.tenders.count,
          filters_count: company.filters.count,
          users_count: company.users.count,
          tariff: company.tariff }

        Sidekiq::Client.push(
          'queue' => 'billing_host_activity',
          'class' => 'Jobs::HostActivityUpdate',
          'args' => [options])
      end
    end
  end
end