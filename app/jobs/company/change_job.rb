class Company
  class ChangeJob < ApplicationJob
    queue_as :company

    def perform(options)
      company = Company.find_by options.slice(:name)

      if options[:tariff]
        company.change_tariff options[:tariff]
        if company.active?
          CompanyMailer.tariff_changed(company.id.to_s, options[:email]).deliver_later
        end
      end

      company.update name: options[:new_name] if options[:new_name]
    end
  end
end