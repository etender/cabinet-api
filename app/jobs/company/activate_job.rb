class Company
  class ActivateJob < ApplicationJob
    queue_as :company

    def perform(options)
      company = Company.find_by options.slice(:name)
      company.set active: true
      company.change_tariff company.tariff

      CompanyMailer.tariff_changed(company.id.to_s, options[:email]).deliver_later

      #TODO: Over time, remove. It is necessary to extend tariff the transferred companies.
      if company.updated_at < Time.parse('2020-04-21 00:00:00 +03')
        company.users.each { |user| user.send_confirmation_instructions }
      end
    end
  end
end