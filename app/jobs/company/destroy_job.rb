class Company
  class DestroyJob < ApplicationJob
    queue_as :company

    def perform(options)
      Company.where(options).destroy_all
    end
  end
end