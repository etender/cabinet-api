class Company
  class DeactivateJob < ApplicationJob
    queue_as :company

    def perform(options)
      company = Company.find_by options.slice(:name)
      company.set active: false
    end
  end
end