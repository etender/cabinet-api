class ApplicationController < ActionController::Base
  include CompanyableController
  include ApiableController
  include AuthenticableController
  include AuthorizableController
end
