class Tender
  class ExportsController < ApplicationController
    respond_to :xlsx

    def create
      @tenders, @total = Tender.with_elastic_search(create_params)
      respond_to do |format|
        format.xlsx {
          response.headers['Content-Disposition'] = 'attachment; filename="tenders.xlsx"'
        }
      end
    end

    private

    def create_params
      params.permit(
        :page, :sort, :status, period: [ :started_at, :ended_at],
        search: [:id, :deal,
                 :pub_date_from, :pub_date_to,
                 :end_date_from, :end_date_to,
                 :stage,
                 :price_from, :price_to, :attachment_search, :has_payment,
                 contact: [], forms: [], areas: [], regions: [], keywords: [], okpds: [], categories: [],
                 federal_law: [], including: [], excluding: []
        ]).merge(user: current_user, per_page: 1000)
    end
  end
end