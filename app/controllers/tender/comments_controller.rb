class Tender
  class CommentsController < ApplicationController

    def index
      comments = Comment.with_search(index_params).page(index_params.fetch :page, 0)
      render json: comments,
             adapter: :json,
             root: 'items',
             meta: { total: comments.count }
    end

    def create
      respond_with tender.comments.create(create_params)
    end

    def update
      Comment.with_search(update_params).update message: update_params[:message]

      head :accepted
    end

    def destroy
      Comment.with_search(destroy_params).destroy
    end

    private

    def index_params
      res = params.permit(:tender_id, :page).merge(company: current_user.company, sort: 'created_at desc')
      res = res.merge(user: current_user) if tender_company_exists?
      res
    end

    def create_params
      params.permit(:message).merge user: current_user, company: current_user.company
    end

    def update_params
      params.permit(:id, :message).merge user: current_user
    end

    def destroy_params
      params.permit(:id).merge user: current_user
    end

    def tender
      @tender ||= Tender.with_search(guid: params[:tender_id], user: current_user).first
    end

    def tender_company_exists?
      @tender_company_exists ||= Tender.with_search(id: params[:tender_id], company: current_user.company).exists?
    end
  end
end