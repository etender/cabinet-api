class OrganizationsController < ApplicationController
  def index
    render json: Organization.with_elastic_search(index_params), each_serializer: OrganizationSerializer
  end

  private

  def index_params
    params.require :search
  end
end