module CompanyableController
  extend ActiveSupport::Concern

  # When they don't have permission to do something
  class InactiveCompany < Exception; end

  included do
    before_action :validate_company!

    rescue_from InactiveCompany do
      head :payment_required
    end
  end

  def validate_company!
    raise InactiveCompany if current_user && !current_user.foreign && (!current_user.company&.active? && !session[:read_only])
  end
end
