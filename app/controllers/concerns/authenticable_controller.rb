# Authenticable controller module class
module AuthenticableController
  extend ActiveSupport::Concern

  included do
    before_action :authenticate_user!
  end
end
