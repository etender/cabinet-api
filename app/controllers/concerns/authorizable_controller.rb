module AuthorizableController
  extend ActiveSupport::Concern

  included do
    before_action :set_read_only
    before_action :validate_read_only!, except: %i[index show]
  end

  protected

  def authenticate_admin_user
    head :forbidden unless current_user.admin?
  end

  def validate_read_only!
    head :forbidden if session[:read_only].present?
  end

  def set_read_only
    current_user.read_only = session[:read_only] if current_user.present?
  end
end
