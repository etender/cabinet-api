module CommentableController
  extend ActiveSupport::Concern

  def comment(tender, message)
    tender.comments.create user: current_user, company: current_user.company, message: message, read_only: true
  end
end
