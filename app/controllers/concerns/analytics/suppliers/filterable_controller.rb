# Filterable controller module class
module Analytics
  module Suppliers
    module FilterableController
      extend ActiveSupport::Concern

      private

      def filter_params
        params.permit(
          :page, :sort, :format,
          search: [:pub_date_from, :pub_date_to, suppliers: [], federal_law: []])
      end
    end
  end
end