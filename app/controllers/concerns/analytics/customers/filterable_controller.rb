# Filterable controller module class
module Analytics
  module Customers
    module FilterableController
      extend ActiveSupport::Concern

      private

      def filter_params
        result = params.permit(
          :page, :sort, :format,
          search: [
            :customer_inn, :deal, :pub_date_from, :pub_date_to, regions: [], federal_law: [], keywords: [],
            including: [], excluding: [], okpds: [], categories: []
          ]).to_h

        result[:search][:customers] = params[:search][:customers]
        result
      end
    end
  end
end