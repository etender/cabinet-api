# Favoriteable controller module class
module FavoritesableController
  extend ActiveSupport::Concern

  def tender
    @tender ||= Tender.with_search(favorites_params).first
  end

  private

  def favorites_params
    params.permit(:favorite_id).merge(user: current_user).tap do |params|
      params[:id] = params.delete :favorite_id
    end
  end
end
