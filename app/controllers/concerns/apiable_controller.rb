# Authenticable controller module class
module ApiableController
  extend ActiveSupport::Concern

  # When they don't have permission to do something
  class InvalidAccess < Exception; end

  included do
    respond_to :json

    # Prevent CSRF attacks by raising an exception.
    protect_from_forgery with: :exception

    # For APIs, you may want to use :null_session instead.
    skip_before_action :verify_authenticity_token, if: :xhr_request?

    rescue_from InvalidAccess do
      head :forbidden
    end
  end

  def invalid_access
    raise InvalidAccess
  end

  protected

  def xhr_request?
    request.xhr?
  end
end
