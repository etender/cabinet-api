# Privateable controller module class
module Api
  module Shared
    module AuthenticableController
      extend ActiveSupport::Concern

      included do
        before_action :authenticate_referer!
      end

      private

      def authenticate_referer!
        domain = (request.headers['X-Referer'] || '')[/^(?:http(?:s)?\:\/\/)?(?:www\.)?([^\/:]*)/i, 1]
        head :forbidden if referers.select { |i| domain == i }.empty?
      end

      def referers
        @referers ||= Rails.application.credentials.dig(Rails.env.to_sym, :api, :shared, :referers) || %w[dev.gartender.ru]
      end
    end
  end
end