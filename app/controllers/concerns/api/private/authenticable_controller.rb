# Privateable controller module class
module Api
  module Private
    module AuthenticableController
      extend ActiveSupport::Concern

      included do
        before_action :authenticate_by_ip!
      end

      private

      def authenticate_by_ip!
        head :forbidden unless allow_ips.include? request.ip
      end

      def allow_ips
        @allow_ips ||= [Rails.application.credentials.dig(Rails.env.to_sym, :api, :private, :ip), '127.0.0.1']
      end
    end
  end
end