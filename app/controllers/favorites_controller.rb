class FavoritesController < ApplicationController
  serialization_scope :view_context

  include CommentableController

  def index
    tenders = Tender.with_search(index_params).includes(:user).page(index_params.fetch :page, 0)
    total_count = tenders.total_count
    tenders = tenders.map do |tender|
      tender.favorites = true
      tender&.browsed = true
      tender
    end

    render json: tenders, adapter: :json, each_serializer: TenderSummarySerializer,
           root: 'items', meta: { total: total_count }
  end

  def show
    tender = Tender.with_search(show_params).first
    tender&.favorites = true
    tender&.browsed = true
    respond_with tender
  end

  def create
    tender = Tender.elastic_find create_params
    if tender.present?
      tender = tender.add_favorites current_user
      tender.favorites = true
      tender.browsed = true

      comment tender, I18n.t('tender.messages.add_favorite')
    end

    respond_with tender
  end

  def destroy
    tender = Tender.with_search(destroy_params).first
    if tender.present?
      comment tender, I18n.t('tender.messages.remove_favorite')
      tender.destroy
    end
  end

  private

  def index_params
    params.permit(:page, :sort, :state, :coordinator, :type).merge(user: current_user)
  end

  def create_params
    params.permit :id
  end

  def show_params
    params.permit(:id).merge user: current_user
  end

  def destroy_params
    params.permit(:id).merge user: current_user
  end
end