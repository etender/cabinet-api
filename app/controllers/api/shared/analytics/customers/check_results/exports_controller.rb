module Api
  module Analytics
    module Shared
      module Customers
        module CheckResults
          class ExportsController < SharedController
            include ::Analytics::Customers::FilterableController

            respond_to :xlsx

            def create
              @check_results = ::Analytics::Customers::CheckResult.with_elastic_search filter_params
              respond_to do |format|
                format.xlsx {
                  response.headers['Content-Disposition'] = 'attachment; filename="check_results.xlsx"'
                }
              end
            end
          end
        end
      end
    end
  end
end