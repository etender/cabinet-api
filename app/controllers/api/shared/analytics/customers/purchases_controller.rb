module Api
  module Shared
    module Analytics
      module Customers
        class PurchasesController < SharedController
          include ::Analytics::Customers::FilterableController

          def index
            purchases, total = ::Analytics::Customers::Purchase.with_elastic_search filter_params
            render json: purchases,
                   adapter: :json,
                   each_serializer: ::Analytics::Customers::PurchaseSerializer,
                   root: 'items',
                   meta: { total: total }
          end
        end
      end
    end
  end
end