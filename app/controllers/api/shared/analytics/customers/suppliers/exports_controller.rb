module Api
  module Analytics
    module Shared
      module Customers
        module Suppliers
          class ExportsController < SharedController
            include ::Analytics::Customers::FilterableController

            respond_to :xlsx

            def create
              @suppliers, @total = ::Analytics::Customers::Supplier.with_elastic_search filter_params.merge(per_page: 1000)
              respond_to do |format|
                format.xlsx {
                  response.headers['Content-Disposition'] = 'attachment; filename="suppliers.xlsx"'
                }
              end
            end
          end
        end
      end
    end
  end
end