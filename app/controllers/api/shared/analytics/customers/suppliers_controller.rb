module Api
  module Shared
    module Analytics
      module Customers
        class SuppliersController < SharedController
          include ::Analytics::Customers::FilterableController

          def index
            supplier, total = ::Analytics::Customers::Supplier.with_elastic_search filter_params
            render json: supplier,
                   adapter: :json,
                   each_serializer: ::Analytics::Customers::SupplierSerializer,
                   root: 'items',
                   meta: { total: total }
          end
        end
      end
    end
  end
end