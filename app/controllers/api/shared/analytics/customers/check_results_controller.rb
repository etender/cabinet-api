module Api
  module Shared
    module Analytics
      module Customers
        class CheckResultsController < SharedController
          include ::Analytics::Customers::FilterableController

          def index
            check_results = ::Analytics::Customers::CheckResult.with_elastic_search filter_params
            respond_with check_results
          end
        end
      end
    end
  end
end