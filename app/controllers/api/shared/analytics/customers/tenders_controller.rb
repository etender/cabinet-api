module Api
  module Shared
    module Analytics
      module Customers
        class TendersController < SharedController
          include ::Analytics::Customers::FilterableController

          def index
            summary = ::Analytics::Customers::Tender.with_elastic_search filter_params

            render json: summary, each_serializer: ::Analytics::Customers::TenderSerializer
          end
        end
      end
    end
  end
end