module Api
  module Shared
    module Analytics
      class FiltersController < SharedController

        def index
        end

        def build
          respond_with ::Analytics::Filter.new build_params
        end

        private

        def build_params
          params.require(:filter)
                .permit :id, :tender_id, :pub_date_from, :pub_date_to, customer_inn: [], regions: [], okpds: [],
                        categories: [], federal_law: [], keywords: [], including: [], excluding: []
        end
      end
    end
  end
end
