module Api
  module Shared
    class ExportsController < SharedController

      respond_to :xlsx

      def create
        @tenders, @total = Tender.with_elastic_search(create_params)
        respond_to do |format|
          format.xlsx {
            response.headers['Content-Disposition'] = 'attachment; filename="tenders.xlsx"'
            render "tender/exports/create.xlsx.axlsx"
          }
        end
      end

      private

      def create_params
        params.permit(
          :page, :sort, :status, period: [:started_at, :ended_at],
          search: [:id, :deal,
                   :pub_date_from, :pub_date_to,
                   :end_date_from, :end_date_to,
                   :stage,
                   :price_from, :price_to, :attachment_search, :has_payment,
                   contact: [], areas: [], regions: [], keywords: [], okpds: [], categories: [],
                   federal_law: [], including: [], excluding: []
          ]).merge(per_page: 1000)
      end
    end
  end
end