module Api
  class Shared::OrganizationsController < SharedController

    def index
      render json: Organization.with_elastic_search(index_params), each_serializer: SuggestionSerializer
    end

    private

    def index_params
      params.require :search
    end
  end
end