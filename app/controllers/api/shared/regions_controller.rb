module Api
  module Shared
    class RegionsController < SharedController

      def index
        respond_with Region.with_search(params).limit(5)
      end

      private

      def index_params
        params.permit :search
      end
    end
  end
end
