module Api
  module Shared
    class OkpdsController < SharedController

      def index
        respond_with Okpd.with_search(params).order_by(code: :asc).limit(30)
      end

      private

      def index_params
        params.permit :id, :parent_id, :search
      end
    end
  end
end
