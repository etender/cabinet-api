module Api
  module Shared
    class SuggestionsController < SharedController

      def index
        render json: Suggestion.with_elastic_search(index_params), each_serializer: SuggestionSerializer
      end

      private

      def index_params
        params.require :search
      end
    end
  end
end