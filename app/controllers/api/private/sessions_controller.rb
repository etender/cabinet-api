module Api
  module Private
    class SessionsController < PrivateController

      def create
        user = User.with_search(create_params).first
        user.present? ?
          render(json: user.sessions.create, serializer: Api::Private::SessionSerializer) :
          head(:not_found)
      end

      private

      def create_params
        params.permit :email
      end
    end
  end
end