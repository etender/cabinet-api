module Api
  module Private
    class UsersController < PrivateController

      def index
        user = User.with_search(index_params).first

        user.present? ?
          respond_with(user, serializer: Api::Private::UserSerializer) :
          head(:not_found)
      end

      private

      def index_params
        params.permit(:email)
      end
    end
  end
end