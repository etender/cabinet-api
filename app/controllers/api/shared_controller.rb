module Api
  class SharedController < ActionController::Base
    include ApiableController
    include Api::Shared::AuthenticableController
  end
end