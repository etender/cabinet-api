module Api
  class PrivateController < ActionController::Base
    include ApiableController
    include Api::Private::AuthenticableController
  end
end