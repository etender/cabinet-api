class FiltersController < ApplicationController
  before_action :rebuild_filter, only: %i[build]
  skip_before_action :validate_read_only!, only: %i[build]

  def index
    invalid_access if !current_user.admin? && params[:user_id].present?

    respond_with Filter.with_search(index_params), each_serializer: FilterSummarySerializer
  end

  def show
    filter = Filter.with_search(show_params.permit(:id)).first
    filter&.rebuild

    respond_with filter
  end

  def create
    respond_with current_user.filters.create create_params
  end

  def update
    respond_with current_filter.update update_params
  end

  def build
    respond_with find_and_apply
  end

  def copy
    respond_with current_filter.copy
  end

  def destroy
    current_filter.destroy
  end

  private

  def index_params
    params.permit(:user_id).merge(user: current_user, user_id: params[:user_id] || current_user.id)
  end

  def show_params
    params.permit(:id).merge user: current_user
  end

  def create_params
    params.require(:filter)
      .permit(:deal, :name,
              :pub_date_from, :pub_date_to,
              :end_date_from, :end_date_to,
              :stage,
              :price_from, :price_to, :attachment_search, :has_payment,
              contact: [], forms: [], areas: [], regions: [], keywords: [], okved: [], okpds: [], categories: [],
              federal_law: [], including: [], excluding: [])
      .merge(company: current_user.company)
  end

  def update_params
    params.require(:filter)
      .permit :deal, :name,
              :pub_date_from, :pub_date_to,
              :end_date_from, :end_date_to,
              :stage,
              :price_from, :price_to, :attachment_search, :has_payment,
              contact: [], forms: [], areas: [], regions: [], keywords: [], okved: [], okpds: [], categories: [],
              federal_law: [], including: [], excluding: []
  end

  def build_params
    params.require(:filter)
      .permit :id, :deal,
              :pub_date_from, :pub_date_to,
              :end_date_from, :end_date_to,
              :stage,
              :price_from, :price_to, :attachment_search, :has_payment,
              contact: [], forms: [], areas: [], regions: [], keywords: [], okved: [], okpds: [], categories: [],
              federal_law: [], including: [], excluding: []
  end

  def find_and_apply
    if build_params[:id].blank? || current_filter.blank?
      return current_user.filters.build build_params
    end

    current_filter.write_attributes build_params
    current_filter
  end

  def rebuild_filter
    current_filter&.rebuild
  end

  def current_filter
    @current_filter ||= Filter.with_search(user: current_user).where(id: params[:id] || params[:filter][:id] || params[:filter_id]).first
    # @current_filter ||= current_user.filters.where(id: params[:id] || params[:filter][:id] || params[:filter_id]).first
  end
end