module Analytics
  class FiltersController < ApplicationController
    def index
    end

    def build
      entity = build_params.require :entity
      entity_class = "Analytics::#{entity.camelize}::Filter".safe_constantize

      filter = build_params[:filter] || {}
      filter_id = filter[:filter_id]
      if filter_id.present?
        filter = ::Filter.where(id: filter_id).first
        filter = filter.as_document.slice(*entity_class.fields.except('_id', 'created_at', 'updated_at').keys) if filter
      end
      filter = entity_class.new filter
      respond_with filter, location: analytics_filters_path
    end

    private

    def build_params
      params.permit(:entity, filter: {})
    end
  end
end