module Analytics
  module Customers
    class TendersController < ApplicationController
      include Analytics::Customers::FilterableController

      def index
        summary = Analytics::Customers::Tender.with_elastic_search filter_params

        render json: summary, each_serializer: Analytics::Customers::TenderSerializer
      end
    end
  end
end
