module Analytics
  module Suppliers
    class SupplierDescriptionController < ApplicationController
      include Analytics::Suppliers::FilterableController

      def index
        summary = Analytics::Suppliers::SupplierDescription.with_elastic_search filter_params
        render json: summary, each_serializer: Analytics::Suppliers::SupplierDescriptionSerializer
      end
    end
  end
end
