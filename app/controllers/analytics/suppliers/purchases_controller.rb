module Analytics
  module Suppliers
    class PurchasesController < ApplicationController
      include Analytics::Suppliers::FilterableController

      def index
        purchases, total = Analytics::Suppliers::Purchase.with_elastic_search filter_params
        render json: purchases,
               adapter: :json,
               each_serializer: Analytics::Suppliers::PurchaseSerializer,
               root: 'items',
               meta: { total: total }
      end
    end
  end
end
