module Analytics
  module Suppliers
    class SupplierDescription
      class ReportsController < ApplicationController

        def create
          response = GardocService.create_report create_params
          service_response response
        end

        def show
          response = GardocService.fetch_report show_params
          service_response response
        end

        private

        def create_params
          params.require(:supplier_number)
        end

        def show_params
          params.require(:id)
        end

        def service_response(response)
          return head :not_acceptable if response.blank?

          return send_data response[:report], filename: response[:filename] if response[:report].present?

          render json: response
        end
      end
    end
  end
end