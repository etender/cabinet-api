module Analytics
  module Suppliers
    module Purchases
      class ExportsController < ApplicationController
        include Analytics::Suppliers::FilterableController

        respond_to :xlsx

        def create
          @purchases, @total = Analytics::Suppliers::Purchase.with_elastic_search filter_params.merge(per_page: 1000)
          respond_to do |format|
            format.xlsx {
              response.headers['Content-Disposition'] = 'attachment; filename="purchases.xlsx"'
            }
          end
        end
      end
    end
  end
end