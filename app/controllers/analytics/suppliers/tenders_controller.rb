module Analytics
  module Suppliers
    class TendersController < ApplicationController
      include Analytics::Suppliers::FilterableController

      def index
        summary = Analytics::Suppliers::Tender.with_elastic_search filter_params
        render json: summary, each_serializer: Analytics::Suppliers::TenderSerializer
      end
    end
  end
end
