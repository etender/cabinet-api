class OtherServicesController < ApplicationController
  def doc_constructor
    # render json: { url: 'http://service.garant.ru/client/constructor/link' }, adapter: :json, root: 'items'

    # response = Net::HTTP.get(URI.parse('http://service.garant.ru/client/constructor/link'))
    response = Net::HTTP.get_response('service.garant.ru', '/client/constructor/link')
    body = response.body.force_encoding("UTF-8")

    if body[/Авторизация/] || response.code != '200'
      return head :unauthorized
    end

    render json: body, adapter: :json
  end
end