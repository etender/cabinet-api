class TimeZonesController < ApplicationController
  def index
    timezones = ActiveSupport::TimeZone.all.uniq { |timezone| timezone.tzinfo.name }
    render json: timezones, adapter: :json, root: 'items', each_serializer: TimeZoneSerializer
  end
end