class UsersController < ApplicationController
  def index
    respond_with User.with_search(index_params).no_active_invitation
  end

  def destroy
    user = User.with_search(destroy_params).first

    if user.tenders.count > 0
      user.errors.add :base, I18n.t('user.with_tender_cannot_be_deleted')
      return render json: user.errors, status: :unprocessable_entity
    end

    if destroy_params[:id].present? && destroy_params[:id] != current_user.id
      user.destroy

      BillingService.change_customer(user.email, user.company.users.where(admin: true).first)
      UserMailer.destroy(email: user.email).deliver_later
    end
  end

  private

  def destroy_params
    { id: params[:id], user: current_user }
  end

  def index_params
    params.permit(:with_filters).merge(user: current_user)
  end
end