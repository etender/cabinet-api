class User
  class RegistrationsController < Devise::RegistrationsController
    respond_to :json

    skip_before_action :validate_company!, only: [:prolong]
    prepend_before_action :authenticate_scope!, only: [:show, :update, :destroy, :extend]
    before_action :configure_sign_up_params, only: [:create]
    before_action :configure_account_update_params, only: [:update]

    def show
      respond_with current_user
    end

    # PUT /resource
     def update
       return super unless params[:current_password] && current_user.foreign?

       head :unauthorized
     end

    def prolong
      unless current_user.company.active?
        CompanyMailer.tariff_extend(current_user.company.id.to_s, current_user.email).deliver_later
      end

      head :accepted
    end

    protected

    # If you have extra params to permit, append them to the sanitizer.
    def configure_sign_up_params
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :time_zone])
    end

    # If you have extra params to permit, append them to the sanitizer.
    def configure_account_update_params
      devise_parameter_sanitizer.permit(
        :account_update,
        keys: [:name, :surname, :patronymic, :phone, :time_zone, :password, :password_confirmation, :current_password,
               :tenders_new_subscription, :tenders_change_subscription])
    end

    def update_resource(resource, params)
      account_update_params[:current_password] ?
        resource.update_with_password(params) :
        resource.update_without_password(params)
    end

  end
end