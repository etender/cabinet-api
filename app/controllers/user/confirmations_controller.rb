class User
  class ConfirmationsController < Devise::ConfirmationsController
    respond_to :json

    def destroy
      current_user.unconfirmed_email = nil
      current_user.save!
    end
  end
end
