class User
  class SessionsController < Devise::SessionsController
    respond_to :json

    skip_before_action :validate_read_only!, only: %i[create destroy]

    def create
      if sign_in_params[:email] && User.where(email: sign_in_params[:email], foreign: true).exists?
        return render json: { error: I18n.t('errors.messages.login_failed') }, status: :unauthorized
      end

      if guid
        response = GarantService.sign_in(guid)
        error = response[:error]
        return head error if error

        user = response[:user]
        sign_in user
        return respond_with user
      end

      if secret
        session = Session.lock secret
        if session.blank?
          return render json: { error: I18n.t('errors.messages.secret_failed') }, status: :unauthorized
        end

        # cookies.signed.permanent[:read_only] = true
        self.session[:read_only] = true

        session.user.read_only = true
        sign_in session.user
        return respond_with session.user
      end

      super
    end

    def destroy
      super { return head :no_content }
    end

    private

    def guid
      @sign_in_guid ||= params[:user] && params[:user][:guid]
    end

    def secret
      @sign_in_secret ||= params[:user] && params[:user][:secret]
    end
  end
end