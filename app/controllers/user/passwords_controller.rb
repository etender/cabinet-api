class User
  class PasswordsController < Devise::PasswordsController
    respond_to :json

    def create
      if password_recovery_params[:email] && User.where(email: password_recovery_params[:email], foreign: true).exists?
        return head :unauthorized
      end

      super
    end

    private

    def password_recovery_params
      params.require(:user).permit(:email)
    end
  end
end
