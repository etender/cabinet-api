class User
class InvitationsController < Devise::InvitationsController
    respond_to :json

    # include AuthorizableController

    before_action :configure_permitted_parameters, only: %i[create update]
    prepend_before_action :authenticate_inviter!, only: %i[create index]
    prepend_before_action :authenticate_admin_user, only: %i[create destroy]

    skip_before_action :require_no_authentication, only: %i[destroy]
    skip_before_action :resource_from_invitation_token, only: %i[destroy]

    def index
      respond_with current_user.company.users.invitation_not_accepted
    end

    def show
      invitation = User.find_by_invitation_token(params[:invitation_token], true)
      if invitation.present?
        respond_with {}
      else
        head :not_acceptable
      end
    end

    def create
      super do |user|
        return head :unauthorized if current_user.foreign?

        if user.errors.present? || !user.company.blank?
          return render json: user.errors, status: :unprocessable_entity
        end

        user.update_attribute :company, user.invited_by.company
        return render json: user
      end
    end

    def destroy
      if destroy_params[:id].present?
        user = User.with_search(destroy_params).first

        if user.invitation_accepted?
          user.errors.add :base, I18n.t('user.cannot_be_deleted')
          return render json: user.errors, status: :unprocessable_entity
        else
          user.destroy
        end
      end
    end

    protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit :accept_invitation, keys: %i[name surname patronymic]
      devise_parameter_sanitizer.permit :invite, keys: %i[name surname patronymic admin coordinator]
    end

    def destroy_params
      params.permit(:id).merge user: current_user
    end
  end
end