class ShortLinksController < ApplicationController
  skip_before_action :authenticate_user!, only: %i[show]
  skip_before_action :verify_authenticity_token, only: %i[show]
  skip_before_action :validate_read_only!, only: %i[create]

  respond_to :html, only: %i[show]

  def show
    link = ShortLink.find_by slug: show_params[:id]
    redirect_to url_prepare(link)
  end

  def create
    link = ShortLink.create create_params
    respond_with link
  end

  private

  def show_params
    params.permit(:id)
  end

  def create_params
    params.permit(:url)
  end

  def url_prepare(link)
    uri = link.url.split(/search#(.*)/)
    params = JSON.parse uri.last

    %w[keywords including excluding contact].each do |field|
      next unless params[field]
      params[field] = params[field].map { |item| URI.escape(ActiveSupport::JSON.encode(item)[1..-2]) }
    end

    uri[1] = params.to_json
    uri.join 'search/#'
  end
end
