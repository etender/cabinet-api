class SuggestionsController < ApplicationController
  def index
    render json: Suggestion.with_elastic_search(index_params), each_serializer: SuggestionSerializer
  end

  def index_params
    params.require :search
  end
end