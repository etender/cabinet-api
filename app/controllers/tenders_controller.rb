class TendersController < ApplicationController
  # Set elastic search model context for serializer
  serialization_scope :view_context

  before_action :send_activity, only: %i[index]

  def index
    tenders, total = Tender.with_elastic_search(index_params)
    render json: tenders,
           adapter: :json,
           each_serializer: TenderSummarySerializer,
           root: 'items',
           meta: { total: total }
  end

  def show
    tender = Tender.elastic_find show_params
    tender&.browsed_by current_user

    respond_with tender
  end

  private

  def index_params
    params.permit(
      :page, :sort, :status, period: [ :started_at, :ended_at],
      search: [:id, :deal,
      :pub_date_from, :pub_date_to,
      :end_date_from, :end_date_to,
      :stage,
      :price_from, :price_to, :attachment_search, :has_payment,
      contact: [], forms: [], areas: [], regions: [], keywords: [], okpds: [], categories: [],
      federal_law: [], including: [], excluding: []
    ]).merge(user: current_user)
  end

  def show_params
    params.permit(:id).merge user: current_user
  end

  def send_activity
    return if current_user.read_only

    Sidekiq::Client.push(
      'queue' => 'billing_host_activity',
      'class' => 'Jobs::HostActivityJob',
      'args' => [
        name: current_user.company.name,
        tariff: current_user.company.tariff
      ]
    )
  end
end