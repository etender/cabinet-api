module Favorites
  class ExportsController < ApplicationController
    respond_to :xlsx

    def create
      @tenders = Tender.with_search(create_params).includes(:user)
      respond_to do |format|
        format.xlsx {
          response.headers['Content-Disposition'] = 'attachment; filename="favorites.xlsx"'
        }
      end
    end

    private

    def create_params
      params.permit(:sort, :state, :coordinator, :type).merge(user: current_user)
    end
  end
end