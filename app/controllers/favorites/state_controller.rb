module Favorites
  class StateController < ApplicationController
    include FavoritesableController
    include CommentableController

    def update
      if tender.present?
        prev = I18n.t "tender.state.#{tender.state}"
        tender.update update_params
        now = I18n.t "tender.state.#{tender.state}"

        comment tender, I18n.t('tender.messages.state_changed', prev: prev, now: now)
      end


      head :accepted
    end

    private

    def update_params
      params.permit(:id).tap do |params|
        params[:state] = params.delete :id
      end
    end
  end
end