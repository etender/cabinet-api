module Favorites
  class UserController < ApplicationController
    include FavoritesableController
    include CommentableController

    def update
      if tender.present? && user.present? && tender.user_id != user.id
        prev = tender.user.full_name
        tender.update(user: user)

        TenderMailer.coordinator_changed(mailer_params).deliver_later
        comment tender, I18n.t('tender.messages.user_changed', prev: prev, now: user.full_name)
      end

      head :accepted
    end

    private

    def update_params
      params.permit(:id).merge user: current_user
    end

    def mailer_params
      params.permit(:id).merge admin_id: current_user.id.to_s, tender_id: tender.id.to_s
    end

    def user
      @coordinator ||= User.with_search(update_params).first
    end
  end
end