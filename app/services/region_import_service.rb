module RegionImportService
  DISTRICTS_PATH = './db/fixtures/regions/districts.yml'.freeze
  REGION_PATH = './db/fixtures/regions/regions.yml'.freeze

  def self.perform
    districts = YAML.load_file DISTRICTS_PATH
    regions = YAML.load_file REGION_PATH

    Region.delete_all

    regions.each do |item|
      value = item['name']
      code = item['code']
      city, region = value.split ', '
      title = city
      title += " (#{region})" if region

      query = code > 100000 ? import_district_query(code, districts) : [import_query(value)]

      Region.create! name: city, title: title, code: item['code'], query: query.compact.join(' OR ')
    end
  end

  def self.import_query(value)
    city, region = value.split ', '

    name = (region ? region : value).downcase.strip.gsub(/республика/i, '').gsub(/область/i, '').gsub(/край/i, '').gsub(/г\./i, '').gsub(/ао/i, '')
    name = name.strip.gsub(/\-/,' AND ').gsub(/(\s*—\s*)/, ' OR ').gsub(/\s{2,}/, ' ')
    name = name.gsub(/(?<!AND|OR)\s(?!AND|OR)/,' AND ')

    query = "(#{region.present? ? city.downcase : name})"

    ext_query = ''
    ext_query << ' NOT (Ямало)' if name == 'ненецкий'
    ext_query << ' OR Кабардино-Балкарская' if name == 'кабардино-балкария'
    ext_query << ' OR Карачаево-Черкесская' if name == 'карачаево-черкесия'
    ext_query << ' OR Удмуртская' if name == 'удмуртия'
    ext_query << ' OR Чеченская' if name == 'чечня'
    ext_query << ' OR Чувашская' if name == 'чувашия'
    query = "(#{query}#{ext_query})"if ext_query.present?

    return query
  end

  def self.import_district_query(code, disctricts)
    district = code.to_s[-1].to_i
    regions = disctricts[district][:regions]

    cache = []

    regions = Region.all.map do |region|
      name = region.name
      name = name.split(',').last.strip if name[/(\,)/].present?

      if regions.include?(region.code)
        next if cache.include? name
        cache << name
        import_query name
      end
    end

    regions.compact
  end
end