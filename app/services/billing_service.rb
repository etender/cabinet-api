class BillingService
  def self.change_customer(email, customer)
    Sidekiq::Client.push(
      'queue' => 'billing_actions',
      'class' => 'Jobs::ChangeCustomerJob',
      'args' =>[{ email: email, customer: customer.to_json }])
  end

  def self.change_customer_email(email, new_email)
    Sidekiq::Client.push(
      'queue' => 'billing_actions',
      'class' => 'Jobs::ChangeCustomerEmailJob',
      'args' =>[{ email: email, new_email: new_email }])
  end
end