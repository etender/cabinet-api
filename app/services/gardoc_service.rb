module GardocService
  class RequestError < StandardError; end

  def self.create_report(number)
    request :post, 'report_requests', query: number
  end

  def self.fetch_report(id)
    response = request :get, "report_requests/#{id}"
    response.merge id: id
  end

  private

  def self.request(method, path, payload = nil)
    url = File.join 'https://proverka.gardoc.ru/api/v1', path

    payload = payload.to_json if payload.present?

    response = RestClient::Request.execute(
      method: method,
      url: url,
      payload: payload,
      headers: {
        # accept: 'application/json',
        content_type: 'application/json',
        'x-api-token': api_token,
      },
      timeout: 360,
      ssl_version: 'SSLv23'
    )
    parse response
  rescue RestClient::ExceptionWithResponse => exception
    parse exception.response
  end

  def self.parse(response)
    if response.blank?
      return {
        error: true,
        status: 500,
        processing: false
      }
    end

    if pdf_response? response
      return {
        report: response.body,
        filename: response.headers[:content_disposition][/filename="([^"]+)"/, 1]
      }
    end

    payload = JSON.parse(response)

    report_request = payload['report_request']
    if report_request.blank?
      return {
        error: true,
        status: response.code,
        processing: false
      }
    end

    if report_request['status'] == 'error'
      status = response.code

      messages = report_request['messages']
      status = 404 if messages && messages[0]['code'] == 15

      return {
        error: true,
        status: status,
        processing: false
      }
    end

    report_request.slice('id', 'retry_after').symbolize_keys.merge(processing: true)
  end

  def self.pdf_response?(response)
    Mime::Type.new(response.headers[:content_type]) =~ Mime[:pdf]
  end

  def self.api_token
    @api_token ||= Rails.application.credentials.dig(Rails.env.to_sym, :gardoc, :token) || 'MjAzMDg6THlqeE01Q2JEdGhUVkN4a3QxOW0'
  end
end
