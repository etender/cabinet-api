module CategoryImportService
  CATEGORIES_PATH = './db/fixtures/categories.yml'.freeze

  def self.perform
    categories = YAML.load_file CATEGORIES_PATH

    Category.delete_all

    time = Time.now.utc
    Category.collection.insert_many(categories.map { |category| category.merge created_at: time, updated_at: time })
  end
end