require 'net/ftp'

module OkpdImportService
  FTP_URL = 'ftp.zakupki.gov.ru'.freeze
  FTP_USERNAME = 'free'.freeze
  FTP_PASSWORD = 'free'.freeze
  FTP_PATH = '/fcs_nsi/nsiOKPD2'.freeze
  OOS_NAMESPACE = 'http://zakupki.gov.ru/oos/types/1'.freeze

  def self.perform
    Okpd.delete_all

    nodes = { root: { _id: 'root', child_ids: [] }}

    Net::FTP.open(FTP_URL, FTP_USERNAME, FTP_PASSWORD, passive: true) do |ftp|
      ftp.passive = true

      ftp.chdir FTP_PATH
      ftp.nlst.each { |file_path| parse nodes, download(ftp, File.basename(file_path)) }
    end

    Okpd.collection.insert_many normalize nodes
  end

  def self.normalize(nodes)
    time = Time.now.utc
    nodes.values.map do |node|
      id = normalize_id nodes, node[:_id]
      child_ids = node[:child_ids].uniq.map { |id| normalize_id nodes, id }
      parent_id = normalize_id nodes, node[:parent_id]
      node.merge _id: id, parent_id: parent_id, created_at: time, updated_at: time, child_ids: child_ids
    end
  end

  def self.normalize_id(nodes, id)
    return id if id.blank? || id.to_s == 'root'

    parent_id = nodes[id.split('.').first][:parent_id]
    return id if parent_id.blank? || parent_id.to_s == 'root'

    [parent_id, id].join '.'
  end

  def self.parse(nodes, file_name)
    puts "Parse file #{file_name}"

    open(file_name) do |file|
      Nokogiri::XML(file.read).xpath('//xmlns:nsiOKPD2').map do |node|
        next unless parse_field(node, 'actual', :boolean)

        id = parse_field node, 'code'
        name = parse_field node, 'name'
        parent_id = parse_field(node, 'parentCode') || :root


        nodes[id] ||= { child_ids: [] }
        nodes[id].merge! _id: id, name: name, parent_id: parent_id

        nodes[parent_id] ||= { child_ids: [] }
        nodes[parent_id][:child_ids].push id

        nodes[id]
      end
    end
  ensure
    File.unlink file_name
  end

  def self.download(ftp, file_name)
    puts "Download file #{file_name}"
    local_file_name = File.join '/tmp', file_name
    ftp.getbinaryfile file_name, local_file_name, 1024

    files = `unzip -o -d #{'/tmp'} #{local_file_name} | awk '{print $NF}'`
    files.strip.split("\n").last
  ensure
    File.unlink local_file_name
  end

  def self.parse_field(node, name, type = :string)
    result = node.at_xpath('oss:' + name, oss: OOS_NAMESPACE)
    return unless result

    result = result.content
    case type
    when :integer
      result.to_i
    when :boolean
      result == 'true'
    else
      result
    end
  end
end