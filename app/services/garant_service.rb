module GarantService
  def self.sign_in(guid)
    Rails.logger.info "[GarantService] Sign in with guid #{guid}."

    response = request "https://service.garant.ru/client/express/service/tender?data=#{guid}"
    return { error: :internal_server_error } if response.blank?

    error = fetch_error response
    return { error: error } if error.present?

    company = find_or_create_company response
    return { error: :not_acceptable } if company.blank?

    user = find_or_create_user response, company
    return { error: :not_acceptable } if user.blank?

    { user: user }
  end

  def self.fetch_error(response)
    error_code = response.dig 'error', 'number'
    return if error_code.blank? || error_code.zero?

    statuses = { '1': :not_found, '2': :forbidden }
    statuses.fetch error_code.to_s, :not_acceptable
  end

  def self.find_or_create_company(response)
    host = response.dig 'client', 'host'
    return if host.blank?

    time = Time.now.utc
    Company
      .where(name: host)
      .find_one_and_update(
        { '$setOnInsert': { tariff: 'endless', created_at: time, updated_at: time, user_limit: 1000} },
        { upsert: true, return_document: :after })
  end

  def self.find_or_create_user(response, company)
    email = create_email response
    user = company.users.where(foreign_email: email).first || company.create_foreign_user(email)
    user.foreign? && user
  end

  def self.create_email(response)
    client = response['client']
    return if client.blank?

    user_name = client['username'].include?('@') ? client['username'].split('@').first : client['username']
    "gsc_#{Digest::SHA1.hexdigest(user_name)}@#{client['host']}.ru"
  end

  def self.request(url)
    return mock_request(url) unless Rails.env.production? || Rails.env.staging?

    url = URI.parse url
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    response = http.get url.request_uri

    if response.code != '200'
      Rails.logger.error "[GarantService] Request exception with http status code: #{response.code}."
      return
    end

    JSON.parse response.body
  end

  def self.mock_request(url)
    {
      client: {
        username: 'foreign@foreign.ru',
        first_name: '',
        last_name: '',
        id_cl: '35-40751-000597',
        tariff: 'endless',
        company: 'foreign',
        host: 'foreign',
        mid_name: ''
      },
      error: {
        number: 0,
        description: ''
      }
    }.deep_stringify_keys
  end
end
