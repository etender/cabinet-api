class CompanyMailer < ApplicationMailer
  def tariff_changed(company_id, email)
    @company = Company.find company_id

    mail to: email, subject: I18n.t('mailers.company.tariff_changed.subject')
  end

  def tariff_extend(company_id, email)
    @company = Company.find company_id
    @user = User.find_by email: email

    mail to: 'gee@garantexpress.ru', subject: I18n.t('mailers.company.tariff_extend.subject')
  end
end