class UserMailer < ApplicationMailer
  def destroy(options)
    mail to: options[:email], subject: I18n.t('mailers.user.destroy.subject')
  end

  def digest(options)
    attachments['Дайджест Гарант Тендер апрель 2020.pdf'] = File.read("#{Rails.root}/public/attachments/Дайджест Гарант Тендер апрель 2020.pdf")
    mail to: options[:email], subject: 'Запуск новой версии Гарант Тендера'
  end
end