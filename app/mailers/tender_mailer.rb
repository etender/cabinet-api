class TenderMailer < ApplicationMailer
  def changes_report(user_id)
    @user = User.find user_id
    return if @user.blank? || !@user.tenders_change_subscription

    @tenders = Tender.with_search(user: @user).with_today_changes

    return if @tenders.count == 0

    Time.use_zone(@user.time_zone) do
      mail to: @user.email, subject: I18n.t('mailers.tender.changes_report.subject', { date: I18n.l(Date.today) })
    end

  end

  def coordinator_changed(options)
    @coordinator = User.find options[:id]
    return if @coordinator.blank?

    @admin = User.find options[:admin_id]

    @tender = Tender.find options[:tender_id]
    return if @tender.blank?

    mail to: @coordinator.email, subject: I18n.t('mailers.tender.coordinator_changed.subject')
  end
end