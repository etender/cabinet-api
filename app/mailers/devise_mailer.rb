class DeviseMailer < Devise::Mailer
  layout 'mailer'

  before_action :add_logo_attachment

  def confirmation_instructions(record, token, options={})
    if record.pending_reconfirmation?
      @token = token
      devise_mail(record, :reconfirmation_instructions, options)
    else
      @password = Devise.friendly_token.first(8)
      @tariff = I18n.t "company.tariffs.#{record.company.tariff}"
      record.update_attributes(password: @password, password_confirmation: @password)

      super
    end
  end

  private

  def add_logo_attachment
    attachments['gartender_logo.png'] = File.read(Rails.root.join('app/assets/images/logo_mailer.png'))
  end
end