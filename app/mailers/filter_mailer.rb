class FilterMailer < ApplicationMailer
  def changes_report(user_id)
    @user = User.find user_id
    return if @user.blank? || !@user.tenders_new_subscription

    @users_filters = Filter.with_search(user: @user).reduce({}) do |memo, filter|
      memo[filter.user_id] ||= []
      memo[filter.user_id].push filter
      memo
    end

    @filters = @users_filters.delete @user.id
    return if @filters.blank?

    @users = User.in id: @users_filters.keys

    tomorrow = Time.now.tomorrow
    @tenders = Tender.active.with_search(user: @user).where(end_at: tomorrow.beginning_of_day..tomorrow.end_of_day)

    @total = @filters.sum &:parsed_count
    return if @total == 0 && @tenders.blank?

    @price = @filters.sum &:parsed_price

    mail to: @user.email, subject: I18n.t('mailers.filter.changes_report.subject', { date: I18n.l(Date.today) })
  end

  def create_first(user_id)
    @user = User.find user_id
    return if @user.blank?

    attachments['clock.png'] = File.read(Rails.root.join('app/assets/images/create_first_filter/clock.png'))
    attachments['letter.png'] = File.read(Rails.root.join('app/assets/images/create_first_filter/letter.png'))
    attachments['search.png'] = File.read(Rails.root.join('app/assets/images/create_first_filter/search.png'))
    attachments['shutterstock.png'] = File.read(Rails.root.join('app/assets/images/create_first_filter/shutterstock.png'))

    mail to: @user.email, subject: I18n.t('mailers.filter.create_first.subject')
  end
end