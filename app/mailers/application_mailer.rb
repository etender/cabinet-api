class ApplicationMailer < ActionMailer::Base
  default from: "Гарант Тендер <noreply@gartender.ru>"

  before_action :get_domain, :add_logo_attachment

  layout 'mailer'

  private

  def domain
    options = CabinetApi::Application.config.action_mailer.default_url_options

    name = "#{options[:protocol]}://#{options[:host]}"
    name = "#{name}:#{options[:port]}" if options[:port]

    name
  end

  def get_domain
    @domain ||= domain
  end

  def add_logo_attachment
    attachments['gartender_logo.png'] = File.read(Rails.root.join('app/assets/images/logo_mailer.png'))
  end
end
