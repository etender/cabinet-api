class Company
  include Documentable
  include Company::Tariffable
  include Company::Userable
  include Company::Invitable
  include Company::Filterable
  include Company::Rootable
  include Company::Tenderable
  include Company::Commentable
  include Company::Restoreable
  include Company::Activeable

  field :name, type: String

  index({ name: 1 })
end