class Region
  module Searchable
    extend ActiveSupport::Concern

    included do
      scope :with_search, lambda { |options|
        with_name(options[:search])
      }

      scope :with_name, lambda { |name|
        where(name: /.*#{Regexp.quote name}.*/i)
      }
    end
  end
end