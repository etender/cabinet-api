module Nameable
  extend ActiveSupport::Concern

  included do
    field :name, type: String
    field :surname, type: String
    field :patronymic, type: String
    field :full_name, type: String

    before_save :update_full_name
  end

  protected

  def update_full_name
    if name
      self.full_name = surname ? "#{surname} #{name[0]}." : name
    else
      self.full_name = surname ? surname : ''
    end

    true
  end
end