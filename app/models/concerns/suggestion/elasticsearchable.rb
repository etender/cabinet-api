class Suggestion
  module Elasticsearchable
    extend ActiveSupport::Concern

    included do
      include Elasticsearch::Model

      index_name ELASTICSEARCH_CONFIG[:suggestions_index]
    end

    module ClassMethods
      def with_elastic_search(search)
        query = {
          query: {
            bool: {
              must: [
                {
                  range: { 'keyword.length' => { lte: 55 }}
                },
                {
                  query_string: {
                    fields: [ 'keyword.raw' ],
                    query: "#{search.gsub(/\s/, '?')}*",
                    boost: 1,
                    analyze_wildcard: true,
                    default_operator: 'AND'
                  }
                }
              ],
              must_not: [
                {
                  wildcard: { 'keyword.raw' => '*,*' }
                }
              ]
            }
          },
          sort: [
            { 'keyword.words_count' => { order: 'asc' }},
            { 'keyword.length' => { order: 'asc' }}
          ],
          from: 0,
          size: 10
        }

        Elasticsearch::Model.search(query, Suggestion).map { |item| Suggestion.new id: item[:_id], title: item[:_source][:keyword]}
      end
    end
  end
end