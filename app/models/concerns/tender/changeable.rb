class Tender
  module Changeable
    extend ActiveSupport::Concern

    CHANGEABLE_FIELDS = %i[name price status end_at start_at auction_at auction_place examination_at
      examination_place extended_to purchase_graph_execution purchase_graph_placing purchase_period
      final_proposals_at final_proposals_place first_part_at open_at open_place prequalification_at
      prequalification_place preselection_at preselection_place]
    LOT_CHANGEABLE_FIELDS = %i[name]

    included do
      field :last_changed_at, type: Time

      # has_many :histories, dependent: :delete_all, primary_key: :guid

      index last_changed_at: 1

      scope :with_today_changes, -> { criteria.gt(last_changed_at: (Time.now - 1.day)).lt(last_changed_at: Time.now) }

      after_destroy :delete_histories
    end

    def histories
      History.where(tender_id: guid, company_id: company_id)
    end

    def last_changes
      histories.gt(created_at: Time.now - 1.day).lt(created_at: Time.now)
    end

    def rebuild_changes
      tender = self.class.elastic_find id: guid
      return if tender.blank?

      write_attributes tender.as_document.except(:_id)

      modifications = self.changes.slice(*CHANGEABLE_FIELDS).merge(rebuild_document_changes)
      modifications = clean_up_times(modifications)

      return if modifications.blank?

      write_attributes last_changed_at: Time.now
      save

      History.create tender_id: guid, company_id: company_id, modifications: modifications
    end

    def rebuild_document_changes
      changes = self.changes[:documents]
      return {} if changes.blank?

      was = serialize_document_changes changes[0]
      become = serialize_document_changes changes[1]

      documents = {}

      added = (become - was).delete_if { |i| i.blank? }
      documents[:added] = added if added.present?

      removed = (was - become).delete_if { |i| i.blank? }
      documents[:removed] = removed if removed.present?

      return {} if documents.empty?

      { documents: documents }
    end

    private

    def serialize_document_changes(document)
      (document || []).map { |item| item.slice(:name) }
    end

    def delete_histories
      histories.delete_all
    end

    def clean_up_times(modifications)
      modifications.select do |key, mod|
        next true unless mod.class == Array

        was =  mod.first
        become = mod.last
        (was.class == Time && become.class == Time && was.beginning_of_minute == become.beginning_of_minute) ? false : true
      end
    end
  end
end