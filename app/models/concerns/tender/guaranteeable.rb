class Tender
  module Guaranteeable
    extend ActiveSupport::Concern

    def guarantee_app
      return if lots.empty? || lots.count > 1 || lots.first.blank? || lots.first[:customer_requirements].blank?

      guarantee_app = lots.first[:customer_requirements].first[:guarantee_app]
      return if guarantee_app.blank?

      amount = guarantee_app[:amount]
      amount if amount.present? && amount.positive?
    end

    def guarantee_contract
      return if lots.empty? || lots.count > 1 || lots.first[:customer_requirements].blank?

      guarantee_contract = lots.first[:customer_requirements].first[:guarantee_contract]
      return if guarantee_contract.blank?

      amount = guarantee_contract[:amount]
      amount if amount.present? && amount.positive?
    end
  end
end