class Tender
  module Browseable
    extend ActiveSupport::Concern

    included do
      attr_accessor :browsed

      has_many :browsings, primary_key: :guid
    end

    def browsed_by(user)
      time = Time.now.utc

      browsings.where(user: user).update(
        {'$set': { updated_at: time }, '$setOnInsert': { created_at: time } },
        { upsert: true })

      self.browsed = true
    end
  end
end