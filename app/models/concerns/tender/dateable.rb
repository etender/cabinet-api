class Tender
  module Dateable
    extend ActiveSupport::Concern
    include ActionView::Helpers::DateHelper
    include ActionView::Helpers::TextHelper
    include ActionView::Helpers::NumberHelper

    included do
      field :published_at, type: Time
      field :modified_at, type: Time
      field :end_at, type: Time
      field :start_at, type: Time
      field :auction_at, type: Time
      field :duration_at, type: Time
      field :examination_at, type: Time
      field :summing_up_at, type: Time
      field :purchase_graph_execution, type: Time
      field :purchase_graph_placing, type: Time
      field :purchase_period, type: Time
    end

    def time_left
      distance_of_time_in_words(Time.now, end_at, except: [:minutes, :seconds], accumulate_on: :days)
    end
  end
end
