class Tender
  module Restoreable
    extend ActiveSupport::Concern

    module ClassMethods
      def restore(options)
        company = Company.where(name: options[:company]).first

        return unless company

        user = company.users.where(email: /#{options[:user]}/i).first
        if user.blank?
          puts "User #{options[:user]} is not in company #{company.name}"
          return
        end

        return unless options[:id]

        tender = nil

        if options[:base_id]
          query = { term: { _id: options[:base_id] }}
          tender = Elasticsearch::Model.search({query: query}, [Tender, PurchasePlan]).first

          tender = elastic_build(tender) if tender
        end

        if tender.blank?
          tender = Tender.elastic_find id: Tender.elastic_build_id(options[:area]||:zakupki_gov_ru, options[:id])
        end

        return if tender.blank?

        return if Tender.where(id: tender.id).exists?

        tender.update user: user, company: company, state: options[:state]

        options.fetch(:comments, []).each do |comment|
          user = User.where(email: comment[:user]).first
          next if user.blank?

          tender.comments.create comment.merge(user: user, company: company)
        end
      end
    end
  end
end