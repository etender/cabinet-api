class Tender
  module Favoritesable
    extend ActiveSupport::Concern

    included do
      attr_accessor :favorites
    end

    def add_favorites(user)
      Tender.create! as_document.except(:_id).merge user: user, company: user.company, state: :valuation
    end
  end
end