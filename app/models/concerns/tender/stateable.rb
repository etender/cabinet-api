class Tender
  module Stateable
    extend ActiveSupport::Concern

    included do
      field :state, type: Symbol

      scope :active, -> { criteria.in(state: [:valuation, :accepted, :participation]) }
    end
  end
end
