class Tender
  module Elasticsearchable
    extend ActiveSupport::Concern

    included do
      include Elasticsearch::Model

      index_name ELASTICSEARCH_CONFIG[:index]
    end

    module ClassMethods

      # Find tender by guid
      def elastic_find(options)
        area, base_id = options[:id].split(':')
        base_id = Base64.decode64 base_id
        query = { bool: { filter: [{ term: { area: area }}, { term: { base_id: base_id.force_encoding('utf-8') }}] } }

        tender = Elasticsearch::Model.search({query: query}, [Tender, PurchasePlan]).first
        return if tender.blank?

        tender = elastic_build tender

        user = options[:user]
        elastic_build_favorites tender, user if user

        tender
      end

      def elastic_find_by_id(id)
        area, base_id = id.split(':')
        base_id = Base64.decode64 base_id
        query = { bool: { filter: [{ term: { area: area }}, { term: { base_id: base_id.force_encoding('utf-8') }}] } }

        tender = Elasticsearch::Model.search({query: query}, [Tender, PurchasePlan]).first
        return if tender.blank?

        elastic_build tender
      end

      def with_elastic_search(options)
        search = options.fetch :search, {}
        id = search[:id]
        search = search.except :id

        filter = find_or_build(id, search, options[:user])
        return if filter.blank?

        filter.attributes = search if search.present?

        page = options.fetch(:page, '1').to_i
        per_page = options.fetch :per_page, 25
        response = filter.search(options).per(per_page).page(page)
        tenders = response.map { |item| elastic_build item }

        if options[:user]
          elastic_build_browsed tenders, options[:user]
          elastic_build_favorites tenders, options[:user]
        end

        return tenders, response.results.total.value
      end

      def elastic_build_browsed(tenders, user)
        tenders = Array tenders
        ids = tenders.map &:id
        browsed = Browsing
                    .where(user: user)
                    .in(tender_id: ids)
                    .pluck(:tender_id)
                    .each_with_object({}) { |id, item| item[id] = true }

        tenders.each { |tender| tender.browsed = browsed.has_key?(tender.id) }
      end

      def elastic_build_favorites(tenders, user)
        tenders = Array tenders
        ids = tenders.map &:id
        favorites = Tender
                      .with_search(guids: ids, company: user.company)
                      .pluck(:guid, :state, :user_id)
                      .each_with_object({users: []}) { |tender, item|
                        item[:users].push tender[2]
                        item[tender[0]] = tender
                      }

        return if favorites.blank?

        users = User
                  .in(id: favorites[:users].uniq)
                  .each_with_object({}) { |user, item| item[user.id] = user }

        tenders.each do |tender|
          favorite = favorites[tender.id]
          if tender.favorites = favorite.present?
            tender.state = favorite[1]
            tender.user = users[favorite[2]]
          end
        end
      end

      def elastic_build(item)
        source = item[:_source]

        organizer = source.fetch(:organizer, {})
        organizer = organizer.merge(source.fetch(:contact, {})).to_h unless organizer.empty?

        customer = source.fetch(:customer, {})
        customer = customer.merge(source.fetch(:contact, {})).to_h if organizer.empty?

        stage = item[:_index] == PurchasePlan.index_name ? :purchase_plan : :tender

        published_at = elastic_build_time source, :published_at
        modified_at = elastic_build_time source, :modified_at
        end_at = elastic_build_time source, :end_at
        created_at = elastic_build_time source, :created_at
        if created_at.blank?
          created_at = published_at || modified_at || end_at
        end

        guid = elastic_build_id(source[:area], source[:base_id])

        new(
          id: guid,
          guid: guid,
          stage: stage,
          origin_id: item[:_id],
          base_id: source[:base_id],
          plan_number: source[:plan_number],
          area: source[:area],
          area_name: AREAS_NAME[source[:area]],
          area_url: AREAS_URL[source[:area]],
          auction_area_name: source[:area_name],
          auction_area_url: source[:area_url],
          federal_law: source[:federal_low],
          federal_law_name: I18n.t("tender.federal_law.#{source[:federal_low]}"),
          region: source[:region],
          url: elastic_build_url(source, stage),
          name: source[:name],
          price: source[:price],
          currency: source[:currency],
          type: source[:type],
          type_name: source[:type_name],
          status: elastic_build_status(source),
          created_at: created_at,
          updated_at: Time.now.utc,
          published_at: published_at || created_at,
          modified_at: modified_at,
          end_at: end_at,
          start_at: elastic_build_time(source, :start_at),
          auction_at: elastic_build_time(source, :auction_at),
          examination_at: elastic_build_time(source, :examination_at),
          summingup_at: elastic_build_time(source, :summingup_at),
          purchase_graph_execution: elastic_build_graph_time(source, :execution, stage),
          purchase_graph_placing: elastic_build_graph_time(source, :placing, stage),
          purchase_period: elastic_build_purchase_period(source, stage),
          customer: customer,
          organizer: organizer,
          lots: prepare_lots(source),
          lots_number: source[:lots].count,
          documents: source[:documents],
          highlight: item[:highlight],
          documents_highlight: item.dig(:inner_hits, :document, :hits, :hits),
          has_payments_highlight: item.dig(:inner_hits, :has_payment, :hits, :hits),
          favorites: false
        )
      end

      def elastic_build_id(area, base_id)
        [area, Base64.encode64(base_id).strip].join':'
      end

      private

      def find_or_build(id, search, user)
        return ::Filter.where(id: id).first if id && search.blank?

        user.filters.build(name: 'filter') if user

        ::Filter.new(name: 'filter')
      end

      def prepare_lots(tender)
        tender.lots = [] unless tender.lots
        tender.lots.map do |lot|
          participants = nil

          lots = tender.protocol&.lots || []
          protocol_lot = lots.find { |item| item.ordinal_number.to_i == lot.ordinal_number.to_i }

          lots = tender.contract&.lots || []
          contract_lot = lots.find { |item| item.ordinal_number.to_i == lot.ordinal_number.to_i }

          suppliers = contract_lot&.suppliers&.select{|i| !i.to_hash.except('address').empty?}

          if protocol_lot && ((protocol_lot.applications && protocol_lot.applications.count > 1) || suppliers.blank?)
            participants = prepare_protocol(protocol_lot, contract_lot&.price)
            lot[:participants_source] = :protocol
          end

          if participants.nil? && suppliers
            participants = prepare_contract(contract_lot)
            lot[:participants_source] = :contract
          end

          lot[:participants] = participants
          lot[:currency] = tender.currency
          lot[:price] = tender.price if lot[:price].blank? && tender.lots.count == 1

          customer_requirements = lot[:customer_requirements]&.first || {}
          guarantee_app = customer_requirements[:guarantee_app] || {}
          guarantee_contract = customer_requirements[:guarantee_contract] || {}

          lot[:tender_security_amount] = guarantee_app[:amount] || 0
          lot[:contract_security_amount] = guarantee_contract[:amount] || 0

          lot
        end
      end

      def prepare_protocol(protocol, contract_price=nil)
        applications = protocol.applications || []
        participants = applications.select { |application| application.application_number }
        participants.map do |participant|
          participant[:winner] = true if participant.application_place.to_i == 1
          participant[:price] = contract_price if participant.winner && participant.price.blank? && contract_price

          participant
        end
      end

      def prepare_contract(contract)
        winner = contract[:suppliers].first
        winner[:price] = contract[:price]
        [winner.merge(winner: true)]
      end

      def elastic_build_status(source)
        status = source[:status]
        status = :completed if Time.parse(source[:end_at]) < Time.now || source[:protocol] || source[:contract]
        status = :canceled if source[:protocol_canceled]
        status = :submission_app unless status

        status
      end

      def elastic_build_url(source, stage)
        if stage == :purchase_plan && source[:federal_low] == 'fl223'
          return "https://zakupki.gov.ru/epz/orderplan/search/results.html?searchString=#{source[:plan_number]}&morphology=on&structuredCheckBox=on&structured=true&notStructured=false&fz223=on&orderPlanSearchPlace=PLANS_AND_DOCUMENTS&currencyId=-1&sortBy=BY_MODIFY_DATE&pageNumber=1&sortDirection=false&recordsPerPage=_10&showLotsInfoHidden=false&searchType=false"
        end
        source[:url]
      end

      def elastic_build_time(source, field)
        value = source[field]
        return if value.blank?

        DateTime.parse(value).in_time_zone.strftime("%FT%T")
      end

      def elastic_build_graph_time(source, field, stage)
        purchase_graph_term_year = source["purchase_graph_#{field}_term_year"]
        if stage == :purchase_plan && purchase_graph_term_year
          Date.new(purchase_graph_term_year, source["purchase_graph_#{field}_term_month"], 1).strftime("%FT%T")
        end
      end

      def elastic_build_purchase_period(source, stage)
        purchaseperiodyear = source[:purchaseperiodyear]
        if stage == :purchase_plan && purchaseperiodyear
          Date.new(purchaseperiodyear, source[:purchaseperiodmonth], 1).strftime("%FT%T")
        end
      end
    end
  end
end