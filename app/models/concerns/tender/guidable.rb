class Tender
  module Guidable
    extend ActiveSupport::Concern

    included do
      field :guid, type: String

      scope :with_guid, ->(guid) { where(guid: guid) if guid.present? }
      scope :with_guids, ->(guids) { criteria.in(guid: guids) if guids.present? }
    end
  end
end
