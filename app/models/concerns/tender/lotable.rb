class Tender
  module Lotable
    extend ActiveSupport::Concern

    def show_lots
      return true if self.lots.count > 1 || self.lots.first.blank?

      lot = self.lots[0]

      if lot[:customer_requirements].blank?
        customer_requirements_is_empty = true
      elsif (lot[:customer_requirements][0].keys - %w(guarantee_app guarantee_contract)).blank?
        customer_requirements_is_empty = true
      else
        customer_requirements_is_empty = false
      end

      !(lot.keys - %w(name price currency ordinal_number customer_requirements)).blank? && !customer_requirements_is_empty
    end
  end
end