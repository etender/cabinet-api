class Tender
  module Searchable
    extend ActiveSupport::Concern

    included do
      scope :with_search, lambda { |options|
        with_guid(options[:guid] || options[:id])
          .with_guids(options[:guids] || options[:ids])
          .with_authorize(options[:user], options[:coordinator])
          .with_stage(options[:type])
          .with_state(options[:state])
          .with_sort(options[:sort])
          .with_company(options[:company])
      }

      scope :with_id, ->(id) { where(id: id) if id.present? }

      scope :with_sort, ->(sort) do
        return if sort.blank?

        field, direction = sort.split(' ')

        order_by("#{field}": direction)
      end

      scope :with_stage, ->(type) {
        where(stage: type == 'tender' ? 'tender' : 'purchase_plan' ) if type.present?
      }

      scope :with_state, ->(state) {
        where(state: state) if state.present?
      }
    end
  end
end