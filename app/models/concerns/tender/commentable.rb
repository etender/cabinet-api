class Tender
  module Commentable
    extend ActiveSupport::Concern

    included do
      has_many :comments, primary_key: :guid
    end
  end
end