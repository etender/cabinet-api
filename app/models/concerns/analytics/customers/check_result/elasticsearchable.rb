module Analytics
  module Customers
    class CheckResult
      module Elasticsearchable
        extend ActiveSupport::Concern

        included do
          include Elasticsearch::Model

          index_name ELASTICSEARCH_CONFIG[:check_results_index]
        end

        module ClassMethods

          def with_elastic_search(options)
            search = options.fetch :search, {}

            aggregation_query = { aggs: { check_results: { terms: { field: 'check_result' } } } }
            %w[complaint unplanned_check].map do |type|
              if type == 'complaint'
                filter = Analytics::Customers::Filter.new search.merge(check_result_type: type)
              elsif type == 'unplanned_check'
                filter = Analytics::Customers::Filter.new search.slice(:customer_inn, :customers, :regions).merge(check_result_type: type)
              end

              response = filter.search(options.merge(aggregation_query), Analytics::Customers::CheckResult).page(0).limit(10)
              new elastic_build(response, type)
            end.compact
          end

          private

          def elastic_build(response, type)
            model = default_model(type, response)

            response.aggregations.check_results.buckets.each do |bucket|
              count = bucket['doc_count'].to_i
              case bucket['key']
              when 'VIOLATIONS', 'COMPLAINT_VIOLATIONS'
                model[:violations_recognized] = model[:violations_recognized] + count
              when 'COMPLAINT_PARTLY_VALID'
                model[:violations_partially_recognized] = count
              when 'NO_VIOLATIONS', 'COMPLAINT_NO_VIOLATIONS', 'COMPLAINT_NOT_MATCHED'
                model[:no_violations] = model[:no_violations] + count
              end
            end
            model
          end

          def default_model(type, response = nil)
            {
              type: type,
              count: response ? response.raw_response['hits']['total']['value'] : 0,
              violations_recognized: 0,
              violations_partially_recognized: 0,
              no_violations: 0
            }
          end
        end
      end
    end
  end
end