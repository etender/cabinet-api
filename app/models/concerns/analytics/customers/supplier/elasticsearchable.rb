module Analytics
  module Customers
    class Supplier
      module Elasticsearchable
        extend ActiveSupport::Concern

        included do
          include Elasticsearch::Model

          index_name ELASTICSEARCH_CONFIG[:purchases_index]
        end

        module ClassMethods

          # Find tender by guid
          def with_elastic_search(options)
            search = options.fetch :search, {}
            return [], 0 unless search[:customer_inn]

            sort = options.fetch :sort, '_count desc'
            field, direction = sort.split(' ')
            field = '_count' if field == 'wins_count'
            field = '_key' if field == 'inn'
            # field = 'name>supplier_name' if field == 'name'

            aggregation_query = { aggs: {
              inn: {
                terms: {
                  field: 'supplier_inn',
                  order: { "_count": "desc" },
                  size: 10
                },
                aggs: {
                  name: { top_hits: { _source: 'supplier_name', size: 1 } },
                  participation_count: { value_count: { field: '_id' } },
                  price: { sum: { field: 'supplier_price' } },
                  price_drop: { avg: { field: 'price_drop' } },
                  "suppliers_bucket_sort": { "bucket_sort": { "sort": [{ "#{field}": { "order": direction } }] }
                  }
                }
              }
            } }

            filter = Analytics::Customers::Filter.new search
            response = filter.search(options.except(:sort).merge(aggregation_query), Analytics::Customers::Supplier)
            suppliers = elastic_build response

            return suppliers, response.results.total.value
          end

          def elastic_build(response)
            response.aggregations.inn.buckets.map do |bucket|
              new(
                inn: bucket['key'],
                name: bucket['name']['hits']['hits'][0]['_source']['supplier_name'],
                participation_count: bucket['participation_count']['value'],
                wins_count: bucket['doc_count'],
                price: bucket['price']['value'],
                price_drop: bucket['price_drop']['value'])
            end
          end
        end
      end
    end
  end
end