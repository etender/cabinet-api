module Analytics
  module Customers
    class Filter
      module CheckResultable
        extend ActiveSupport::Concern

        included do
          attr_accessor :check_result_type
        end

        def build_query(query = {})
          query = super query

          if check_result_type.present?
            query[:bool][:must] << check_result_query
          end

          query
        end

        def check_result_query
          {
            bool: {
              filter: [
                { term: { check_type: check_result_type } },
                { exists: { field: 'check_result' } }
              ]
            }
          }
        end
      end
    end
  end
end