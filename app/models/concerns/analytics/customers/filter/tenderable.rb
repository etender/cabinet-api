module Analytics
  module Customers
    class Filter
      module Tenderable
        extend ActiveSupport::Concern

        included do
          attr_accessor :tender_id

          after_initialize :apply_tender, if: :tender_id
        end

        private

        def apply_tender
          tender = ::Tender.elastic_find_by_id tender_id

          customer_inn = tender.customer['inn']
          self.customer_inn = customer_inn if customer_inn.present?

          customer_name = tender.customer['name']
          self.customer_name = customer_name if customer_name.present?

          self.keywords = keywords.map { |keyword| keyword.gsub(/#{Regexp.escape tender.base_id}(\s+)?/, '') }.filter { |keyword| keyword.present? } if keywords
          self.including = including.map { |keyword| keyword.gsub(/#{Regexp.escape tender.base_id}(\s+)?/, '') }.filter { |keyword| keyword.present? } if including
          self.excluding = excluding.map { |excluding| excluding.gsub(/#{Regexp.escape tender.base_id}(\s+)?/, '') }.filter { |keyword| keyword.present? } if excluding

          if pub_date_to.blank? && pub_date_from.blank?
            self.pub_date_to = Time.now
            self.pub_date_from = pub_date_to - 1.year
          end

          true
        end
      end
    end
  end
end