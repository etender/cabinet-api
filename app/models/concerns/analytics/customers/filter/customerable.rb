module Analytics
  module Customers
    class Filter
      module Customerable
        extend ActiveSupport::Concern

        included do
          attr_accessor :customer_name
          attr_accessor :customers

          field :customer_inn, type: String
        end

        def build_query(query = {})
          query = super query

          if customers.present?
            query[:bool][:must] << customers_query
          end

          if customer_inn.present?
            query[:bool][:must] << { term: { customer_inn: customer_inn } }
          end

          query
        end

        def customers_query
          query = { bool: { should: [], minimum_should_match: 1 }}

          customer_inns = customers.map { |customer| customer.is_a?(String) ? customer : customer[:inn] }.compact.uniq
          query[:bool][:should] << { terms: { customer_inn: customer_inns } }

          customer_names = customers.map { |customer| customer.is_a?(String) ? customer : customer[:title] }.compact.uniq.join(', ')
          query[:bool][:should] << search_query(customer_names, %w(customer_name))

          query
        end
      end
    end
  end
end