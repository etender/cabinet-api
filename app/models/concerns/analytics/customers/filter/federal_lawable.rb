module Analytics
  module Customers
    class Filter
      module FederalLawable
        extend ActiveSupport::Concern

        included do
          field :federal_law, type: Array
        end

        def build_query(query = {})
          query = super query

          return query unless federal_law.present?
          query[:bool][:must] << { terms: { "#{federal_law_field}": federal_law } } if federal_law.present?

          query
        end
      end
    end
  end
end