module Analytics
  module Customers
    class Tender
      module Elasticsearchable
        extend ActiveSupport::Concern

        included do
          include Elasticsearch::Model

          index_name ELASTICSEARCH_CONFIG[:purchases_index]
        end

        module ClassMethods

          def with_elastic_search(options)
            search = options.fetch :search, {}
            # return [] unless search[:customer_inn] || search[:customers]

            aggregation_query = {
              aggs: {
                average_price: {
                  avg: { field: 'supplier_price', missing: 0 }
                },
                average_suppliers_count: {
                  filter: {
                    bool: {
                      must_not: [
                        {
                          term: { type: :ep }
                        }
                      ]
                    }
                  },
                  aggs: {
                    avg_suppliers_count: {
                      avg: { field: 'suppliers_count' }
                    }
                  }
                },
                average_price_drop: {
                  filter: {
                    bool: {
                      must_not: [
                        {
                          term: { type: :ep }
                        }
                      ]
                    }
                  },
                  aggs: {
                    avg_price_drop: {
                      avg: { field: 'price_drop' }
                    }
                  }
                }
              }
            }

            filter = Analytics::Customers::Filter.new search
            response = filter.search(options.merge(aggregation_query), Analytics::Customers::Tender)

            new active_tenders: active_tenders(options),
                average_price: { value: response.aggregations.average_price.value&.round || 0 },
                average_suppliers_count: { value: response.aggregations.average_suppliers_count.avg_suppliers_count.value&.round(1) || 0 },
                average_price_drop: { value: response.aggregations.average_price_drop.avg_price_drop.value&.round(1) || 0, unit: "%" }
          end

          private

          def active_tenders(options)
            search = options.fetch(:search, {}).to_hash
            customer_inn = search.delete 'customer_inn'
            customers = search.delete 'customers'

            if customer_inn.present?
              search[:contact] = [customer_inn]
            end

            if customers.present?
              search[:contact] = customers
                                   .map { |customer| customer.is_a?(String) ? customer : customer[:title] }
                                   .compact
                                   .uniq
            end

            aggregation_query = {
              aggs: {
                price: {
                  sum: { field: 'price' }
                }
              }
            }

            filter = ::Filter.new search
            response = filter.search(options.merge(aggregation_query), ::Tender)
            { value: response.results.total.value, sum: response.aggregations.price&.value || 0 }
          end
        end
      end
    end
  end
end