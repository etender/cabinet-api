module Analytics
  module Suppliers
    class Tender
      module Elasticsearchable
        extend ActiveSupport::Concern

        included do
          include Elasticsearch::Model

          index_name ELASTICSEARCH_CONFIG[:suppliers_index]
        end

        module ClassMethods

          def with_elastic_search(options)
            search = options.fetch :search, {}

            aggregation_query = {
              aggs: {
                participation_count:{
                  filter: {
                    bool: {
                      should: [
                        {
                          bool: {
                            must_not: [
                              {
                                bool: {
                                  should: [
                                    { term: { app_not_considered: true }},
                                    { term: { app_admitted: false }}
                                  ],
                                  minimum_should_match: 1
                                }
                              }
                            ]
                          }
                        },
                        {
                          term: { source: "contract" }
                        }
                      ],
                      minimum_should_match: 1
                    }
                  }
                },
                contract_sum: {
                  filter: {
                    bool: {
                      must: [
                        { term: { winner: true }}
                      ]
                    }
                  },
                  aggs: {
                    contract_sum: {
                      sum: {
                        field: "supplier_price",
                        missing: 0
                      }
                    }
                  }
                },
                average_price_drop: {
                  filter: {
                    bool: {
                      must_not: [
                        { term: { type: "ep" }}
                      ],
                      must: [
                        { term: { winner: true }}
                      ]
                    }
                  },
                  aggs: {
                    avg_price_drop: {
                      avg: {
                        field: "price_drop"
                      }
                    }
                  }
                },
                customers_count: {
                  filter: {
                    bool: {
                      must: [
                        { term: { winner: true }}
                      ]
                    }
                  },
                  aggs: {
                    customers_count:{
                      cardinality: {
                        field: "customer_inn"
                      }
                    }
                  }
                },
                average_suppliers_count: {
                  avg: {
                    field: "supplier_count"
                  }
                }
              }
            }

            filter = Analytics::Suppliers::Filter.new search
            response = filter.search(options.merge(aggregation_query), Analytics::Suppliers::Tender)

            new contracts: {value: response.aggregations[:contract_sum][:doc_count] || 0, sum: response.aggregations[:contract_sum][:contract_sum].value || 0},
                participation_count: {value: response.aggregations[:participation_count][:doc_count]},
                average_price_drop: {value: response.aggregations.average_price_drop.avg_price_drop.value&.round(1) || 0, unit: "%"},
                customers_count: {value: response.aggregations.customers_count.customers_count.value || 0},
                average_suppliers_count: {value: response.aggregations.average_suppliers_count.value&.round(1) || 0},
                participation_admission: {value: response.response.hits.total.value ? ((response.aggregations[:participation_count][:doc_count].to_f * 100) / response.response.hits.total.value.to_f).round(1) : 0, unit: "%"}
          end
        end
      end
    end
  end
end