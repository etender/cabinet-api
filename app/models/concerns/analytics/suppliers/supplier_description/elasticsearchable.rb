module Analytics
  module Suppliers
    class SupplierDescription
      module Elasticsearchable
        extend ActiveSupport::Concern

        included do
          include Elasticsearch::Model

          index_name ELASTICSEARCH_CONFIG[:suppliers_index]
        end

        module ClassMethods

          def with_elastic_search(options)
            search = options.fetch :search, {}
            options = {
              inn: search.fetch(:suppliers, []).first,
              name: nil,
              kpp: nil,
              ogrn: nil }

            filter = Analytics::Suppliers::Filter.new search
            response = filter.search(options, Analytics::Suppliers::SupplierDescription)
            response = response.response.hits.hits.first
            if response.present?
              supplier = response["_source"]
              options.merge!(
                name: supplier['supplier_name'],
                kpp: supplier['supplier_kpp'],
                ogrn: supplier['supplier_ogrn'])
            end

            new options
          end

          private

          def participation_count(options)
            filter = ::Filter.new search
            response = filter.search(options.merge(aggregation_query), ::Tender)
            { value: response.results.total.value, sum: response.aggregations.price&.value || 0 }
          end
        end
      end
    end
  end
end