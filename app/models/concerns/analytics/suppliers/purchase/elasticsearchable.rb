module Analytics
  module Suppliers
    class Purchase
      module Elasticsearchable
        extend ActiveSupport::Concern

        included do
          include Elasticsearch::Model

          index_name ELASTICSEARCH_CONFIG[:purchases_index]
        end

        module ClassMethods

          # Find tender by guid
          def with_elastic_search(options)
            search = options.fetch :search, {}
            filter = Analytics::Suppliers::Filter.new search

            page = options.fetch(:page, '0').to_i + 1
            per_page = options.fetch :per_page, 5
            response = filter.search(options, Analytics::Suppliers::Purchase).per(per_page).page(page)
            tenders = response.map { |item| elastic_build item }

            return tenders, response.results.total.value
          end

          def elastic_build(item)
            source = item[:_source]
            options = source.slice(
              :tender_name,
              :tender_url,
              :published_at,
              :supplier_price,
              :supplier_inn,
              :supplier_name,
              :customer_name).to_hash
            options[:id] = ::Tender.elastic_build_id(source[:area], source[:tender_base_id])
            options[:price_drop] = (100 - (source[:supplier_price].to_f / source[:lot_price].to_f) * 100).round(2)
            # options[:price_drop] = ((1 - source[:supplier_price].to_i / source[:lot_price].to_f) * 100).round(2)

            new options
          end
        end
      end
    end
  end
end