module Analytics
  module Suppliers
    class Filter
      module Searchable
        extend ActiveSupport::Concern

        included do
          include ::Searchable
          include ::Filter::Aggregateable
          include ::Filter::Publishable
          include ::Filter::Keywordsable
          include ::Filter::Regionable
          include ::Filter::Periodable
          include ::Filter::Codesable
          include ::Filter::Categoriable
          include Analytics::Customers::Filter::FederalLawable
          include Analytics::Suppliers::Filter::Supplierable
        end

        def federal_law_field
          @federal_law_field ||= 'federal_law'
        end

        def keywords_fields
          @keywords_fields ||= %w[tender_name lot_name lot_items_name description]
        end

        def regions_fields
          @regions_fields ||= %w[tender_name region customer_post_address contact_fact_address delivery_place]
        end

        def codes_okved_fields
          @codes_okved_fields ||= %w[lot_items_okved lot_items_okved2]
        end

        def codes_okpds_fields
          @codes_okpds_fields ||= %w[lot_items_okpd2]
        end

        def categories_fields
          @codes_okpds_fields ||= %w[lot_items_okpd2]
        end
      end
    end
  end
end