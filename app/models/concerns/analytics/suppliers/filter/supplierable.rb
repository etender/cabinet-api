module Analytics
  module Suppliers
    class Filter
      module Supplierable
        extend ActiveSupport::Concern

        included do
          attr_accessor :suppliers
        end

        def build_query(query = {})
          query = super query

          if suppliers.present?
            query[:bool][:must] << { terms: { supplier_inn: suppliers } }
          end

          query[:bool][:must] << { range: { published_at: { gte: (Time.now - 1.year).utc.strftime('%F %T UTC') }}}

          query
        end
      end
    end
  end
end
