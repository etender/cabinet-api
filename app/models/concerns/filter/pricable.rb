class Filter
  module Pricable
    extend ActiveSupport::Concern

    included do
      field :price_from, type: BigDecimal
      field :price_to, type: BigDecimal
    end

    def build_query(query = {})
      query = super query

      return query unless price_from&.positive? || price_to&.positive?

      query[:bool][:must] << price_query

      query
    end

    def price_query
      query = {}

      if price_from&.positive?
        query[:from] = price_from
        query[:include_lower] = true
      end

      if price_to&.positive?
        query[:to] = price_to
        query[:include_upper] = true
      end

      {range: {price: query}}
    end
  end
end