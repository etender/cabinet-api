class Filter
  module Regionable
    extend ActiveSupport::Concern

    included do
      field :regions, type: Array
    end

    def build_query(query = {})
      query = super query

      return query unless regions.present?

      query[:bool][:must] << {
        :query_string => {
          fields: regions_fields,
          query: region_collection_prepare(regions),
          type: "phrase",
          quote_field_suffix: ".exact",
          default_operator: 'AND'
        }
      }
      query
    end

    def region_prepare(region)
      region = region[:name].mb_chars.downcase.strip

      if region[/республика/i]
        name = region.gsub(/республика/i, '').strip
        region = "(#{name} респ) OR (респ #{name}) OR (республика #{name})"
      elsif region[/область/i]
        name = region.gsub(/область/i, '').strip
        region = "(#{name} область) OR (#{name} обл) OR (обл #{name})"
      elsif region[/край/i]
        name = region.gsub(/край/i, '').strip
        region = "(#{name} край) OR (край #{name})"
      elsif region[/[\s\-—]+ао/i]
        name = region.gsub(/ао/i, '').strip
        region = "(#{name} ао) OR (#{name} окр) OR (#{name} округ) OR (#{name} автономный округ)"
      else
        region = "(#{region})"
      end

      region.include?('ё') ? "#{region} OR #{region.gsub('ё', 'е')}" : region
    end

    def regions_prepare(regions)
      extra_regions = []
      regions = regions.map do |region|
        case region.name
          when /кабардино-балкария/i
            extra_regions << {name: "кабардино-балкарская республика", code: region.code}
          when /карачаево-черкесия/i
            extra_regions << {name: "карачаево-черкесская республика", code: region.code}
          when /удмуртия/i
            extra_regions << {name: "удмуртская республика", code: region.code}
          when /чечня/i
            extra_regions << {name: "чеченская республика", code: region.code}
          when /чувашия/i
            extra_regions << {name: "чувашская республика", code: region.code}
          when /ханты.{,3}мансийский/i
            extra_regions << {name: "ханты мансийский ао", code: region.code}
            extra_regions << {name: "югра", code: region.code}
            extra_regions << {name: "ХМАО", code: region.code}
        end
        region.attributes.slice(:code, :name).deep_symbolize_keys
      end

      regions + extra_regions
    end

    def region_collection_prepare(region_codes)
      regions_expanded = []
      regions = Region.where(:code.in => region_codes)
      regions = regions_prepare(regions)
      regions.each do |region|
        if region[:code].to_i < 100000
          regions_expanded << region_prepare(region)
        else
          regions_expanded << district_prepare(region)
        end
      end

      regions_expanded.flatten.join(' OR ')
    end

    def district_prepare(region)
      district = region[:code].to_s[-1].to_i
      regions = YAML.load(File.read File.join(Rails.root, 'config', 'districts.yml'))[district][:regions]

      Region.where(:code.in => regions).map { |region| region_prepare region }
    end
  end
end