class Filter
  module Highlightable
    def search_payload(options)
      query = super options
      query.merge highlight: highlight
    end

    def highlight
      { fields: {
        'name' => { fragment_size: 1000 },
        'name.exact' => { fragment_size: 1000 },
        'lot_items_name' => { fragment_size: 160 },
        'lot_items_name.exact' => { fragment_size: 160 },
        'lots_name' => { fragment_size: 160 },
        'lots_name.exact' => { fragment_size: 160 },
        'products.okpd2_name' => { :fragment_size => 160 },
        'products.okpd2_code' => { :fragment_size => 160 },
        'description' => { fragment_size: 160 },
        'description.exact' => { fragment_size: 160 },
        'lots.customer_requirements.delivery_place' => { fragment_size: 100 },
        'customer.fact_address' => { fragment_size: 100 },
        'customer.post_address' =>  { fragment_size: 100 },
        'contact.fact_address' => { fragment_size: 100 },
        'contact.post_address' => { fragment_size: 100 },
        'region' => { fragment_size: 100 }
      }}
    end
  end
end