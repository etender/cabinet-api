class Filter
  module Modifiedable
    extend ActiveSupport::Concern

    included do
      field :modified_date_from, type: DateTime
      field :modified_date_to, type: DateTime
    end

    def build_query(query = {})
      query = super query

      return query unless modified_date_from.present? || modified_date_to.present?

      query[:bool][:must] << modified_query

      query
    end

    def modified_query
      query = { range: { modified_at: {} } }
      query[:range][:modified_at][:gte] = modified_date_from.beginning_of_day.utc.to_s if modified_date_from.present?
      query[:range][:modified_at][:lte] = modified_date_to.end_of_day.utc.to_s if modified_date_to.present?

      query
    end
  end
end