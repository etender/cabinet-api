class Filter
  module Periodable
    extend ActiveSupport::Concern

    def build_root_query(options)
      query = super options
      build_period_query query, options[:period]
    end

    def build_period_query(query, options)
      return query if options.blank?

      started_at = options[:started_at]
      ended_at = options[:ended_at]

      return query if started_at.blank? && ended_at.blank?

      query[:bool][:must] << period_query(started_at, ended_at)

      query
    end

    def period_query(started_at, ended_at)
      query = {}
      query[:gte] = Time.parse(started_at).utc.to_s if started_at.present?
      query[:lte] = Time.parse(ended_at).utc.to_s if ended_at.present?

      { range: { published_at: query } }
    end
  end
end