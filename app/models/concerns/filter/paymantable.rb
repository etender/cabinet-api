class Filter
  module Paymantable
    extend ActiveSupport::Concern

    included do
      field :has_payment, type: Boolean
    end

    def build_query(query = {})
      query = super query

      return query if !has_payment.present? || purchase_plan_stage?

      query[:bool][:must] << {
        bool: {
          should: [
            { exists: { field: "lots.customer_requirements.advance_payment_sum_percent" }},
            { exists: { field: "lots.customer_requirements.advance_payment_sum_value" }},
            {
              has_child: {
                type: 'document',
                query: has_payment_query,
                inner_hits: {
                  _source: {
                    exclude: ['attachment.content'],
                    include: ['attachment.highlight']
                  },
                  name: 'has_payment',
                  highlight: { fields: { 'attachment.content.base': { fragment_size: 100} } }
                }
              }
            }
          ],
          minimum_should_match: 1
        }
      }

      query
    end

    def has_payment_query
      {
        bool: {
          "must": [
            {
              "span_near": {
                "clauses": [
                  {
                    "span_or": {
                      "clauses": [
                        {
                          "span_term": {
                            "attachment.content.base": "предоплата"
                          }
                        },
                        {
                          "span_term": {
                            "attachment.content.base": "аванс"
                          }
                        },
                        {
                          "span_term": {
                            "attachment.content.base": "авансирование"
                          }
                        },
                        {
                          "span_term": {
                            "attachment.content.base": "авансовый"
                          }
                        },
                        {
                          "span_term": {
                            "attachment.content.base": "предварительная оплата"
                          }
                        },
                        {
                          "span_term": {
                            "attachment.content.base": "первый первичный транш"
                          }
                        }
                      ]
                    }
                  },
                  {
                    "span_near": {
                      "clauses": [
                        {
                          "span_or": {
                            "clauses": [
                              { "span_term": { "attachment.content.base": "5" }},
                              { "span_term": { "attachment.content.base": "10" }},
                              { "span_term": { "attachment.content.base": "15" }},
                              { "span_term": { "attachment.content.base": "20" }},
                              { "span_term": { "attachment.content.base": "25" }},
                              { "span_term": { "attachment.content.base": "30" }},
                              { "span_term": { "attachment.content.base": "35" }},
                              { "span_term": { "attachment.content.base": "40" }},
                              { "span_term": { "attachment.content.base": "45" }},
                              { "span_term": { "attachment.content.base": "50" }},
                              { "span_term": { "attachment.content.base": "55" }},
                              { "span_term": { "attachment.content.base": "60" }},
                              { "span_term": { "attachment.content.base": "65" }},
                              { "span_term": { "attachment.content.base": "70" }},
                              { "span_term": { "attachment.content.base": "75" }},
                              { "span_term": { "attachment.content.base": "80" }},
                              { "span_term": { "attachment.content.base": "85" }},
                              { "span_term": { "attachment.content.base": "90" }},
                              { "span_term": { "attachment.content.base": "95" }},
                              { "span_term": { "attachment.content.base": "100" }}
                            ]
                          }
                        },
                        {
                          "span_term": { "attachment.content.base": "%" }
                        }
                      ],
                      "slop": 1,
                      "in_order": true
                    }
                  }
                ],
                "slop": 2,
                "in_order": false
              }
            }
          ],
          "must_not":[
            {
              "span_near": {
                "clauses": [
                  {
                    "span_or": {
                      "clauses": [
                        {
                          "span_term": {
                            "attachment.content.base": "предоплата"
                          }
                        },
                        {
                          "span_term": {
                            "attachment.content.base": "аванс"
                          }
                        },
                        {
                          "span_term": {
                            "attachment.content.base": "авансирование"
                          }
                        },
                        {
                          "span_term": {
                            "attachment.content.base": "авансовый"
                          }
                        },
                        {
                          "span_term": {
                            "attachment.content.base": "предварительная оплата"
                          }
                        },
                        {
                          "span_term": {
                            "attachment.content.base": "первый первичный транш"
                          }
                        }
                      ]
                    }
                  },
                  {
                    "span_or":{
                      "clauses": [
                        {
                          "span_term": {
                            "attachment.content.base": "не"
                          }
                        },
                        {
                          "span_term": {
                            "attachment.content.base": "без"
                          }
                        },
                        {
                          "span_term": {
                            "attachment.content.base": "если"
                          }
                        },
                        {
                          "span_term": {
                            "attachment.content.base": "исключено"
                          }
                        }
                      ]
                    }
                  }
                ],
                "slop": 3,
                "in_order": false
              }
            }
          ]
        }
      }
    end
  end
end