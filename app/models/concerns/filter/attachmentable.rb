class Filter
  module Attachmentable
    extend ActiveSupport::Concern

    included do
      field :attachment_search, type: Boolean, default: true
    end

    def build_query(query = {})
      query = super query

      return query unless attachment_search? && has_keywords? && !purchase_plan_stage?

      query[:bool][:should] << { has_child: {
        type: 'document',
        query: attachment_keywords_query(%w(attachment.content)),
        inner_hits: {
          name: 'document',
          _source: {
            excludes: %w(attachment.content)
          },
          highlight: {
            fields: {
              'attachment.content': { fragment_size: 150 },
              'attachment.content.exact': { fragment_size: 150 }
            }
          }
        }
      }}

      query[:bool][:minimum_should_match] = 1
      query
    end

    def attachment_keywords_query(fields, boost = 1)
      query = { bool: { must: [], must_not: [] }}

      query[:bool][:must] << keywords_condition(keywords_prepare(keywords), fields) if keywords.present?
      query[:bool][:must] << keywords_condition(keywords_prepare(including), fields) if including.present?
      query[:bool][:must_not] << keywords_condition(keywords_prepare(excluding), fields) if excluding.present?

      query
    end
  end
end