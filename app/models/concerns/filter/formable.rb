class Filter
  module Formable
    extend ActiveSupport::Concern

    included do
      field :forms, type: Array
    end

    def build_query(query = {})
      query = super query

      unless forms.present?
        query[:bool][:must_not] << { :term => { :type => 'ep' } }
        return query
      end

      query[:bool][:must] << forms_query

      query
    end

    def forms_query
      query = { bool: { should: []} }

      query[:bool][:should] << { terms: { type: forms }}
      query[:bool][:should] << search_query(forms.map { |form| FORMS[form] }.join(', '), %w(type_name))

      query
    end
  end
end