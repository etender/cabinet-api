class Filter
  module Aggregateable
    def search_payload(options)
      query = super options

      aggs = options[:aggs]
      query[:aggs] = aggs if aggs.present?

      query
    end
  end
end