class Filter
  module Publicable
    extend ActiveSupport::Concern

    included do
      field :public, type: Boolean, default: false
      field :share_key, type: String

      scope :with_public, lambda { |public = nil| where public: public.present? }
    end
  end
end