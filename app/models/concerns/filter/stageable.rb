class Filter
  module Stageable
    extend ActiveSupport::Concern

    included do
      field :stage, type: String, default: 'application'
    end

    def purchase_plan_stage?
      @purchase_plan_stage ||= stage == 'plan'
    end

    def build_query(query = {})
      query = super query

      return query unless stage.present?

      stage == 'archival' ? build_archival_query(query) : build_active_query(query)
    end

    def build_archival_query(query)
      time = Time.now.beginning_of_day.gmtime.to_s
      query[:bool][:filter] = [
        {
          bool:{
            should:[
              { exists: { field: 'protocol_canceled' }},
              { exists: { field: 'protocol' }},
              { range: { end_at: { lt: time } } },
              { terms: { status: %w(completed canceled commission) } }
            ],
            minimum_should_match: '1'
          }
        }
      ]

      query
    end

    def build_active_query(query)
      time = Time.now.beginning_of_day.gmtime.to_s
      query[:bool][:must_not] << { terms: { status: %w(completed canceled commission) } }
      query[:bool][:filter] =  [
        {
          bool:{
            must_not:[
              bool:{
                should:[
                  { exists: { field: 'protocol_canceled' } },
                  { exists: { field: 'protocol' } }
                ],
                minimum_should_match: '1'
              }
            ]
          }
        },
        { range: { end_at: { gte: time } } }
      ]

      query
    end
  end
end