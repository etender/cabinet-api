class Filter
  module Quantityable
    extend ActiveSupport::Concern

    included do
      after_create :calculate_quantity
      after_destroy :calculate_quantity
    end

    def calculate_quantity
      self.user.update_attribute(:filters_quantity,  self.user.filters.count)
    end
  end
end