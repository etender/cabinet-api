class Filter
  module Endable
    extend ActiveSupport::Concern

    included do
      field :end_date_from, type: DateTime
      field :end_date_to, type: DateTime
    end

    def build_query(query = {})
      query = super query

      return query unless end_date_from.present? || end_date_to.present?

      query[:bool][:must] << end_query

      query
    end

    def end_query
      query = {range: {end_at: {}}}
      query[:range][:end_at][:gte] = end_date_from.beginning_of_day.utc.to_s if end_date_from.present?
      query[:range][:end_at][:lte] = end_date_to.end_of_day.utc.to_s if end_date_to.present?

      query
    end
  end
end