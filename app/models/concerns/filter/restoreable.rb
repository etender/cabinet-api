class Filter
  module Restoreable
    extend ActiveSupport::Concern

    module ClassMethods
      def restore(options)
        company = Company.where(name: options[:company]).first

        return unless company

        user = company.users.where(email: /#{options[:user]}/i).first
        if user.blank?
          puts "User #{options[:user]} is not in company #{company.name}"
          return
        end

        company.filters.create! options.merge(
          user: user,
          okved: restore_codes(options[:okved]),
          okpds: restore_codes(options[:okpds]),
          categories: restore_categories(options[:categories])
        )
      end

      def restore_codes(values)
        return if values.blank?

        values.map { |value| Okpd.where(id: /^[A-Z]\.#{Regexp.quote value}$/i).first&.id }
      end

      def restore_categories(values)
        return if values.blank?

        @categories ||= {}

        values.map do |value|
          if @categories[value].blank?
            id = Category.where(id: /#{Regexp.quote value}$/i).first&.id
            @categories[value] = id
          end

          @categories[value]
        end
      end
    end
  end
end