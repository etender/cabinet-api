class Filter
  module Searchable
    extend ActiveSupport::Concern

    included do
      include ::Searchable
      include Filter::Keywordsable
      include Filter::Attachmentable
      include Filter::FederalLawable
      include Filter::Contactable
      include Filter::Innable
      include Filter::Codesable
      include Filter::Categoriable
      include Filter::Regionable
      include Filter::Formable
      include Filter::Areable
      include Filter::Pricable
      include Filter::Paymantable
      include Filter::Publishable
      include Filter::Endable
      include Filter::Modifiedable
      include Filter::Stageable
      include Filter::Periodable
      include Filter::Statusable
      include Filter::Highlightable

      scope :with_search, lambda { |options|
        with_authorize(options[:user], options[:user_id])
          .with_public(options[:public])
          .with_id(options[:id])
          .collation(locale: 'ru', strength: 2)
          .order_by(name: :asc)
      }

      scope :with_id, ->(id) { where(id: id) if id.present? }
    end

    def federal_law_field
      @federal_law_field ||= 'federal_low'
    end

    def keywords_fields
      @keywords_fields ||= %w[name lot_items_name lots_name description base_id]
    end

    def regions_fields
      @regions_fields ||= %w[lots.customer_requirements.delivery_place name customer.post_address contact.post_address region]
    end

    def codes_okved_fields
      @codes_okved_fields ||= %w[lots.lot_items.okved lots.lot_items.okved2 products.okved2_code]
    end

    def codes_okpds_fields
      @codes_okpds_fields ||= %w[lots.lot_items.okpd2 products.okpd2_code]
    end

    def search_model
      purchase_plan_stage? ? PurchasePlan : Tender
    end

    def categories_fields
      @codes_okpds_fields ||= purchase_plan_stage? ? %w[products.okpd2_code] : %w[lots.lot_items.okpd2]
    end
  end
end