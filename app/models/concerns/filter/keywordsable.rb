class Filter
  module Keywordsable
    extend ActiveSupport::Concern

    included do
      field :keywords, type: Array
      field :including, type: Array
      field :excluding, type: Array
    end

    def build_query(query = {})
      query = super query

      return query unless has_keywords?

      if keywords.present?
        query[:bool][:should] << keywords_condition(keywords_prepare(keywords), keywords_fields)
        query[:bool][:minimum_should_match] = 1
      end

      query[:bool][:must] << keywords_condition(keywords_prepare(including), keywords_fields) if including.present?
      query[:bool][:must_not] << keywords_condition(keywords_prepare(excluding), keywords_fields) if excluding.present?

      query
    end

    def has_keywords?
      keywords.present? || excluding.present? || including.present?
    end

    def keywords_condition(items, fields)
      keywords_query = []

      if items[:without_quotes].present?
        keywords_query << search_query(items[:without_quotes].join(', '), fields)
      end

      if items[:with_quotes].present?
        keywords_query << phrase_query(items[:with_quotes].join(', '), fields - %w(base_id customer.inn))
      end

      { bool: { should: keywords_query, minimum_should_match: 1 } }
    end

    def keywords_prepare(items)
      result = { without_quotes: [],  with_quotes: [] }

      items.each do |keyword|
        next if keyword.strip.empty?
        
        if keyword.include? '"'
          result[:with_quotes] << keyword.gsub(/\A['"]+|['"]+\Z/, "")
        else
          result[:without_quotes] << keyword
        end
      end

      result
    end
  end
end