class Filter
  module Innable
    extend ActiveSupport::Concern

    included do
      field :inn, type: String
    end

    def build_query(query = {})
      query = super query

      return query unless inn.present?

      query[:bool][:must] << { terms: { 'customer.inn': inn.split(', ') } }

      query
    end
  end
end