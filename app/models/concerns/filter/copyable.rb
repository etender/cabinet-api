class Filter
  module Copyable
    extend ActiveSupport::Concern

    # Copy filter and replace created and updated dates
    def copy
      filter = clone
      time = Time.current

      filter.write_attributes name: copy_name, created_at: time, updated_at: time
      filter.save
      filter
    end

    def copy_name
      index = name[/^\(копия\s(\d+)\)/i, 1]&.to_i
      index ? name.sub(/^\(копия\s(\d+)\)/i, "(копия #{index + 1})") : "(копия 1) #{name}"
    end
  end
end