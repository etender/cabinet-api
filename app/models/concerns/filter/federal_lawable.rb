class Filter
  module FederalLawable
    extend ActiveSupport::Concern

    included do
      field :federal_law, type: Array
      field :deal, type: Boolean
    end

    def build_query(query = {})
      query = super query

      return query unless federal_law.present? || deal.present?

      subquery = { bool: { should: [] }}

      if deal
        subquery[:bool][:should] << { match: { deal: 'bankruptcy' }}
      else
        query[:bool][:must_not] << { match: { deal: 'bankruptcy' }}
      end

      if federal_law.present? && federal_law.include?('smpchs')
        subquery[:bool][:should] << { term: { small_purchase: true }}
      else
        query[:bool][:must_not] << { term: { small_purchase: true }}
      end

      subquery[:bool][:should] << { terms: { "#{federal_law_field}": federal_law - ['smpchs'] }} if federal_law.present?

      return query if subquery[:bool][:should].empty?

      if subquery[:bool][:should].size == 1
        query[:bool][:must] << subquery[:bool][:should][0]
      else
        subquery[:bool][:minimum_should_match] = 1
        query[:bool][:must] << subquery
      end

      query
    end
  end
end