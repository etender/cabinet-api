class Filter
  module Statusable
    extend ActiveSupport::Concern

    def build_root_query(options)
      query = super options
      build_status_query query, options[:status]
    end

    def build_status_query(query, status)
      case status
      when 'new'
        query[:bool][:must] << { range: { created_at: { gt:  (Time.now - 3.days).utc.to_s } } }
      when 'winner'
        query[:bool][:must] << { term: { 'protocol.lots.applications.application_place' => 1 } }
      when 'canceled'
        query[:bool][:must] << { term: { status: 'canceled' } }
      end

      query
    end
  end
end