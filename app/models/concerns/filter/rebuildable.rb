class Filter
  module Rebuildable
    extend ActiveSupport::Concern

    included do
      field :parsed_count, type: Integer, default: 0
      field :parsed_price, type: Integer, default: 0
      field :count, type: Integer, default: 0

      after_create :rebuild
      after_update :rebuild
    end

    def rebuild
      payload = {
        track_total_hits: true,
        query: build_query,
        sort: sort_by,
        aggs: {
          parsed: {
            filter: {
              range: {
                created_at: { gte: Time.now.utc.yesterday.strftime('%F %T UTC') } } },
            aggs: {
              price: { sum: { field: 'price' } } }
          }
        }
      }

      model =  purchase_plan_stage? ? PurchasePlan : Tender
      begin
        response = Elasticsearch::Model.search(payload, model).response
      rescue  Exception => e
        Rails.logger.error "[FilterRebuildError] #{e.message}"
        e.backtrace.each { |line| Rails.logger.error line }
      end

      set parsed_count: response ? response.aggregations.parsed.doc_count : 0,
          parsed_price: response ? response.aggregations.parsed.price.value : 0,
          count: response ? response.hits.total.value : 0
    end
  end
end