class Filter
  module Areable
    extend ActiveSupport::Concern

    included do
      field :areas, type: Array
    end

    def build_query(query = {})
      query = super query

      return query if !areas.present? && !user&.company&.tariff_is_light?

      query[:bool][:must] << area_query

      query
    end

    def area_query

      if user && user.company.tariff_is_light?
        return { terms: { area: [:zakupki_gov_ru] } }
      end

      query = { bool: { should: []} }

      items = areas.select { |area| !::FEDERAL_AREAS.include?(area) }
      query[:bool][:should] << { terms: { area: items } } if items.present?

      items = areas.select { |area| ::FEDERAL_AREAS.include?(area) }
      query[:bool][:should] << { terms: { parser: items } } if items.present?

      if query[:bool][:should].size == 1
        return query[:bool][:should].first
      end

      query[:bool][:minimum_should_match] = 1

      query
    end
  end
end