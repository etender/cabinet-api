class Filter
  module Publishable
    extend ActiveSupport::Concern

    included do
      field :pub_date_from, type: DateTime
      field :pub_date_to, type: DateTime
    end

    def build_query(query = {})
      query = super query

      return query unless pub_date_from.present? || pub_date_to.present?

      query[:bool][:must] << publish_query

      query
    end

    def publish_query
      query = { range: { published_at: {} } }
      query[:range][:published_at][:gte] = pub_date_from.beginning_of_day.utc.to_s if pub_date_from.present?
      query[:range][:published_at][:lte] = pub_date_to.end_of_day.utc.to_s if pub_date_to.present?

      query
    end
  end
end