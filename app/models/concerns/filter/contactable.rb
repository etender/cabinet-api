class Filter
  module Contactable
    extend ActiveSupport::Concern

    included do
      field :contact, type: Array
    end

    def build_query(query = {})
      query = super query

      return query unless contact.present?

      fields = %w(contact.name customer.name lots.customer_requirements.customer_name customer.inn)
      query[:bool][:must] << search_query(contact_prepare, fields)

      query
    end

    private

    def contact_prepare
      contact.join(', ').gsub(/["']/, '').gsub(/[\+\-\&\|\!\(\)\{\}\[\]\^\~\*\?\:\\]/,' ')
    end
  end
end