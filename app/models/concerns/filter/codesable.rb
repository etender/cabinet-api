class Filter
  module Codesable
    extend ActiveSupport::Concern

    included do
      field :okved, type: Array
      field :okpds, type: Array
    end

    def build_query(query = {})
      query = super query

      return query unless okved.present? || okpds.present?

      query[:bool][:must] << codes_query

      query
    end

    def codes_query
      query = { bool: { should: [], minimum_should_match: 1 }}

      if okved.present?
        codes = code_prepare okved
        codes_okved_fields.each do |code_field|
          query[:bool][:should] << { regexp: { "#{code_field}": codes }}
        end
      end

      if okpds.present?
        okpd_prepare(okpds).each_slice(30) do |codes|
          codes = code_prepare codes
          codes_okpds_fields.each do |code_field|
            query[:bool][:should] << { regexp: { "#{code_field}": codes }}
          end
        end
      end

      query
    end

    def okpd_prepare(codes)
      parent_codes = Okpd.where(parent_id: :root).map(&:id)
      codes = codes.map do |item|
        if parent_codes.include? item
          Okpd.find(item).children.map{|children| Okpd.normalize children.id}
        else
          Okpd.normalize item
        end
      end.flatten

      codes
    end

    def code_prepare(codes)
      codes = codes.map { |code| [code, '*'].join('.') }.join('|')
      "(#{codes})"
    end
  end
end