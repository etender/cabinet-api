class Filter
  module Categoriable
    extend ActiveSupport::Concern

    included do
      field :categories, type: Array
    end

    def build_query(query = {})
      query = super query

      return query unless categories.present?

      query[:bool][:must] << categories_query

      query
    end

    def categories_query
      query = { bool: { should: [], minimum_should_match: 1 }}

      categories_codes.each_slice(30) do |codes|
        codes = code_prepare codes
        categories_fields.each do |code_field|
          query[:bool][:should] << { regexp: { "#{code_field}": codes }}
        end
      end

      query
    end

    def categories_codes
      deserialized_categories.map { |category| category[:codes] }.flatten.uniq
    end

    def deserialized_categories
      @deserialized_categories ||= Category.in(id: categories).to_a
    end
  end
end