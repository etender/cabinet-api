module TreeViewSearchable
  extend ActiveSupport::Concern

  included do
    scope :with_search, lambda { |options|
      with_id(options[:id])
        .with_parent(options[:parent_id])
        .with_name(options[:search])
    }

    scope :with_id, lambda { |id|
      return if id.blank?

      if id == 'root'
        criteria.or({ id: id}, { parent_id: id })
      else
        where(id: id)
      end
    }

    scope :with_parent, ->(parent_id) { where(parent_id: parent_id) if parent_id.present? }

    scope :with_name, lambda { |search|
      return if search.blank?

      criteria
        .or({name: /.*#{Regexp.quote search }.*/i},
            {id: /[A-Z]\.#{Regexp.quote search }.*/i})
        .order_by(id: :asc)
    }
  end
end