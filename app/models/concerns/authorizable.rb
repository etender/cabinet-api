module Authorizable
  extend ActiveSupport::Concern

  included do
    # With authorize scope return entities. if user is admin then return company entities else user entities.
    # user_id can be used for change user search context. Only admin can change user search context.
    scope :with_authorize, lambda { |user, user_id = nil|
      return if user.blank?

      if user.admin?
        return where(user_id: user_id) if user_id.present?
        return where(company: user.company)
      end

      where(user: user)
    }
  end
end