module Companyable
  extend ActiveSupport::Concern

  included do
    belongs_to :company, index: true, optional: true

    scope :with_company, ->(company) {
      where(company: company) if company
    }
  end
end