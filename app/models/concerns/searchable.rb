module Searchable
  extend ActiveSupport::Concern

  def search(options, model = nil)
    payload = search_payload options
    optimize_search_payload payload
    model = model || search_model
    Elasticsearch::Model.search payload, model
  end

  def build_query(query = {})
    query.merge bool: { should: [], must: [], must_not: [] }
  end

  def build_root_query(options = {})
    build_query
  end

  def optimize_search_payload(payload)
    if payload.is_a? Array
      return payload.each { |payload| optimize_search_payload payload }
    end

    return unless payload.is_a? Hash

    payload.delete_if { |key, value| %i[must should must_not].include?(key) && value.empty? }

    payload.values.each { |value| optimize_search_payload value }
  end

  def search_payload(options)
    { track_total_hits: true, query: build_root_query(options), sort: sort_by(options[:sort]), size: (0 if options[:aggs]) }.compact
  end

  def search_query(query, fields, boost = 1, operator = 'OR')
    {
      :query_string => {
        fields: fields,
        query: query_string(query, operator),
        boost: boost,
        quote_field_suffix: ".exact",
        default_operator: 'AND'
      }
    }
  end

  def phrase_query(query, fields, boost = 1, operator = 'OR')
    {
      :query_string => {
        fields: fields,
        query: query_string(query, operator),
        boost: boost,
        type: "phrase",
        quote_field_suffix: ".exact",
        default_operator: 'AND'
      }
    }
  end

  def query_string(query, operator)
    res = query.gsub(/(\d+),(\d{1,2}(?!\d))/, '\1~\2')
    res = res.gsub('(', '\\(').gsub(')', '\\)')
    res = res.gsub('»', '"').gsub('«', '"').gsub('“', '"').gsub('”', '"').gsub(':', '\\:').gsub('+', '\\+').gsub('~', ',')
    res = res.gsub('/', '\\/').gsub('\\', '\\\\')
    res = res.split(',').map{|sp| sp.count('"').even? ? sp.strip : sp.sub('"','').strip}
    res = res.reject { |item| item.empty? }

    "(" + res.join(") #{operator} (") + ")"
  end

  def sort_by(params = nil)
    return [{ published_at: { order: 'desc' } }, '_score'] if params.blank? || params == 'rank'

    params = params.split(' ')
    type = params[0] == 'price' ? 'long' : 'date'
    [{ "#{params.first}": { order: params.last, unmapped_type: type } }, '_score']
  end
end