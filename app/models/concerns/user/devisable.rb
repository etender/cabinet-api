class User
  module Devisable
    extend ActiveSupport::Concern

    included do
      devise :database_authenticatable, :recoverable, :trackable, :invitable, :validatable, :rememberable, :confirmable,
             :registerable

      ## Database authenticatable
      field :email,              type: String, default: ''
      field :encrypted_password, type: String, default: ''

      ## Recoverable
      field :reset_password_token,   type: String
      field :reset_password_sent_at, type: Time

      ## Rememberable
      field :remember_created_at, type: Time

      ## Trackable
      field :sign_in_count,      type: Integer, default: 0
      field :current_sign_in_at, type: Time
      field :last_sign_in_at,    type: Time
      field :current_sign_in_ip, type: String
      field :last_sign_in_ip,    type: String

      ## Confirmable
      field :confirmation_token,   type: String
      field :confirmed_at,         type: Time
      field :confirmation_sent_at, type: Time
      field :unconfirmed_email,    type: String

      ## Invitable
      field :invitation_token, type: String
      field :invitation_created_at, type: Time
      field :invitation_sent_at, type: Time
      field :invitation_accepted_at, type: Time
      field :invitation_limit, type: Integer

      has_many :invitations, class_name: self.to_s, as: :invited_by

      index({ invitation_token: 1 }, background: true)
      index({ invitation_by_id: 1 }, background: true)
    end
  end
end
