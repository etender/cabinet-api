class User
  module Companyable
    extend ActiveSupport::Concern

    included do
      belongs_to :company, index: true, optional: true, inverse_of: :users
      belongs_to :invitation_company, class_name: 'Company', inverse_of: :invitations, index: true, optional: true
    end
  end
end