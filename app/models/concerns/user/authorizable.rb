class User
  module Authorizable
    extend ActiveSupport::Concern

    included do
      attr_accessor :read_only

      field :developer, type: Boolean, default: false
      field :coordinator, type: Boolean, default: false
      # Role for garant service user
      field :foreign, type: Boolean, default: false
      field :foreign_email, type: String, default: false

      scope :with_authorize, lambda { |user|
        where(company: user.company) if user.present?
      }
    end
  end
end