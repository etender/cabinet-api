class User
  module Gravatarable
    extend ActiveSupport::Concern

    def avatar_image_url
      @avatar_image_url ||= Gravatar.new(self.email).image_url( ssl: true, size:80, default: :mp )
    end
  end
end