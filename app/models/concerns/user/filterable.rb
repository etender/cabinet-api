class User
  module Filterable
    extend ActiveSupport::Concern

    included do
      has_many :filters, dependent: :delete_all
    end
  end
end