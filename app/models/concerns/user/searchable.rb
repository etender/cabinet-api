class User
  module Searchable
    extend ActiveSupport::Concern

    included do
      scope :with_search, lambda { |options|
        with_id(options[:id])
          .with_email(options[:email])
          .with_authorize(options[:user])
          .with_filters(options[:with_filters], options[:user])
      }

      scope :with_id, ->(id) { where(id: id) if id.present? }
      scope :with_email, ->(email) { where(email: email) if email.present? }
      scope :with_filters, ->(with_filters, user) { gt(filters_quantity: 0).ne(id: user.id) if with_filters.present? }
    end
  end
end