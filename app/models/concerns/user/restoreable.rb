class User
  module Restoreable
    extend ActiveSupport::Concern

    module ClassMethods
      def restore(options)
        if User.where(email: /^#{options[:email]}$/i).exists?
          File.write('/tmp/missed_users.txt', "#{options[:email]} - #{options[:company]}\n", mode: 'a')
          return
        end

        company = Company.find_by name: options[:company]
        password = Devise.friendly_token.first(8)

        user = User.new options.merge(password: password, password_confirmation: password)
        user.company = company
        user.skip_confirmation!

        user.save!
      end
    end
  end
end