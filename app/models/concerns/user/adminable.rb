class User
  module Adminable
    extend ActiveSupport::Concern

    included do
      field :admin, type: Boolean, default: false
    end

    module ClassMethods
      def admin
        User.find_by email: admin_email
      end

      def admin_email
        @admin_email ||= Rails.application.credentials.dig(Rails.env.to_sym, :admin, :email) || 'admin@dev.gartender.ru'
      end
    end
  end
end