class User
  module Subscribable
    extend ActiveSupport::Concern

    included do
      field :tenders_change_subscription, type: Boolean, default: true
      field :tenders_new_subscription, type: Boolean, default: true
      field :email_changed_at, type: Time

      before_create :clean_subsciption, if: :foreign
      before_update :update_email_changed_at, if: :foreign

      scope :with_tenders_change_subscription, -> { where(tenders_change_subscription: true) }
      scope :with_tenders_new_subscription, -> { where(tenders_new_subscription: true) }
    end

    def update_email_changed_at
      self.email_changed_at = Time.now if email_changed?

      true
    end

    def clean_subsciption
      self.tenders_change_subscription = false
      self.tenders_new_subscription = false
    end
  end
end