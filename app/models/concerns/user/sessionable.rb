class User
  module Sessionable
    extend ActiveSupport::Concern

    included do
      has_many :sessions, dependent: :delete_all
    end
  end
end