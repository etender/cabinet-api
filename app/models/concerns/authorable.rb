module Authorable
  extend ActiveSupport::Concern

  included do
    embeds_one :author

    before_save :cache_author
  end

  private

  def cache_author
    self.author = Author.new(
      email: user.email,
      name: user.name,
      surname: user.surname,
      patronymic: user.patronymic,
      full_name: user.full_name,
      avatar_image_url: user.avatar_image_url,
      created_at: user.created_at,
      updated_at: user.updated_at)
    true
  end
end