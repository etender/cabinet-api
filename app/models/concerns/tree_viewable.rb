module TreeViewable
  extend ActiveSupport::Concern

  included do
    include TreeViewSearchable

    field :name, type: String
    field :parent_id, type: String

    belongs_to :parent, class_name: self.name, index: true
    has_and_belongs_to_many :children, class_name: self.name, index: true

    index name: 1
  end
end