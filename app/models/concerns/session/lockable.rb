class Session
  module Lockable
    extend ActiveSupport::Concern

    module ClassMethods
      def lock(secret)
        Session.where(secret: secret).find_one_and_delete
      end
    end
  end
end