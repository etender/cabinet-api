class Session
  module Secretable
    extend ActiveSupport::Concern

    included do
      field :secret, type: String

      before_create :generate_secret

      index({ secret: 1 }, unique: true)
    end

    private

    def generate_secret
      self.secret = Devise.friendly_token
    end
  end
end
