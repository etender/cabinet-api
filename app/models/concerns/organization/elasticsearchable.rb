class Organization
  module Elasticsearchable
    extend ActiveSupport::Concern

    included do
      include Elasticsearch::Model

      index_name ELASTICSEARCH_CONFIG[:organization_index]
    end

    module ClassMethods
      def with_elastic_search(search)
        query = {
          query: {
            bool:{
              should:[
                {
                  query_string: {
                    fields: %w[inn kpp ogrn short_name full_name],
                    query: "#{search.gsub(/[\+\-\&\|\!\(\)\{\}\[\]\^\"\~\*\?\:\\]/,' ')}*",
                    analyze_wildcard: true,
                    default_operator: 'AND'
                  }
                }
              ],
              minimum_should_match: 1
            }
          },
          from: 0,
          size: 10
        }

        Elasticsearch::Model.search(query, Organization).map { |item| elastic_build item }

        # autocomplite with suggest
        #
        # query = {
        #   suggest: {
        #     organizations_suggest: {
        #       prefix: search,
        #       completion: {
        #         field: "suggest",
        #         size: 10
        #       }
        #     }
        #   }
        # }
        #
        # res = Elasticsearch::Model.search(query, Organization)
        # res.response.suggest.organizations_suggest.first.options.map do |item|
        #   elastic_build item
        # end
      end

      def elastic_find_by_ids(ids)
        return if ids.blank?

        query = { terms: { _id: ids } }
        response = Elasticsearch::Model.search({query: query}, Organization)
        response.map { |item| elastic_build item }
      end

      def elastic_find_by_customers(customers)
        return if customers.blank?

        query = {
          bool: {
            should: customers.map do |customer|
              { term: { inn: { value: customer } } }
            end
          }
        }

        response = Elasticsearch::Model.search({query: query}, Organization)
        response.map { |item| elastic_build item }
      end

      def elastic_build(item)
        source = item[:_source]

        new(
          id: item[:_id],
          title: source[:full_name],
          inn: source[:inn],
          sub_title: "ИНН: #{source[:inn]}, КПП: #{source[:kpp]}",
          created_at: source[:created_at],
          updated_at: source[:updated_at]
        )
      end
    end
  end
end