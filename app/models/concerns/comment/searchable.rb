class Comment
  module Searchable
    extend ActiveSupport::Concern

    included do
      scope :with_search, lambda { |options|
        with_id(options[:id])
          .with_tender(options[:tender_id])
          .with_authorize(options[:user])
          .with_company(options[:company])
          .with_sort(options[:sort])
      }

      scope :with_id, ->(id) { where(id: id) if id.present? }

      scope :with_sort, lambda { |sort|
        return if sort.blank?

        field, direction = sort.split(' ')

        order_by("#{field}": direction)
      }

      scope :with_authorize, lambda { |user|
        return if user.blank?

        any_of({user: user}, {read_only: true}) unless user.admin?
      }
    end
  end
end