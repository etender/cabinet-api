module Tenderable
  extend ActiveSupport::Concern

  included do
    belongs_to :tender, index: true, primary_key: :guid

    scope :with_tender, ->(id) { where(tender_id: id) if id.present? }
  end
end