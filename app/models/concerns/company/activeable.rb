class Company
  module Activeable
    extend ActiveSupport::Concern

    included do
      field :active, type: Boolean, default: true

      scope :active, -> { where active: true }
    end
  end
end