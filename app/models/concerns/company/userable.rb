class Company
  module Userable
    extend ActiveSupport::Concern

    included do
      has_many :users, dependent: :delete_all
    end

    def create_foreign_user(email)
      user = users.new(
        email: email,
        foreign_email: email,
        password: Devise.friendly_token.first(8),
        name: 'noname',
        surname: 'noname',
        patronymic: 'noname',
        time_zone: 'Europe/Moscow',
        coordinator: true,
        foreign: true)
      user.skip_confirmation!
      user.save
      user
    end
  end
end