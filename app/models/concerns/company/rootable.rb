class Company
  module Rootable
    extend ActiveSupport::Concern

    module ClassMethods
      def root
        Company.find_by name: root_name
      end

      def root_name
        root_name ||= Rails.application.credentials.dig(Rails.env.to_sym, :company, :name) || 'Гартендер'
      end
    end
  end
end