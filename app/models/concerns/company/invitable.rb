class Company
  module Invitable
    extend ActiveSupport::Concern

    included do
      has_many :invitations, class_name: 'User', inverse_of: :invitation_company, dependent: :nullify
    end
  end
end