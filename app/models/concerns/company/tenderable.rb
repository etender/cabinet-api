class Company
  module Tenderable
    extend ActiveSupport::Concern

    included do
      has_many :tenders, dependent: :delete_all
    end
  end
end