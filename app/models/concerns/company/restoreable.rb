class Company
  module Restoreable
    extend ActiveSupport::Concern

    module ClassMethods
      def restore(options)
        return if Company.where(name: options[:name]).exists?

        Company.create_with_tariff options.slice(:name, :tariff, :active, :created_at, :updated_at)
      end
    end
  end
end