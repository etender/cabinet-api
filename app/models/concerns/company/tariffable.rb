class Company
  module Tariffable
    extend ActiveSupport::Concern

    included do
      field :tariff, type: String
      field :tariff_end_at, type: Time
      field :user_limit, type: Integer

      scope :expired, -> { criteria.lt tariff_end_at: Time.now.utc }
    end

    module ClassMethods
      def create_with_tariff(company={})
        tariff = TARIFFS[company[:tariff].to_sym]
        Company.create! company.merge({tariff_end_at: Time.now + tariff[:period], user_limit: tariff[:user_limit]})
      end
    end

    def change_tariff(code)
      tariff = TARIFFS[code.to_sym]
      self.update tariff: code, tariff_end_at: Time.now + tariff[:period], user_limit: tariff[:user_limit]
    end
  end

  def tariff_name
    @tariff_name ||= I18n.t "company.tariffs.#{tariff}"
  end

  def tariff_is_light?
    %w(light_full light_base).include? tariff
  end
end