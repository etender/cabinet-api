class Suggestion
  include Documentable
  include Suggestion::Elasticsearchable

  field :title, type: String
end