class Organization
  include Documentable
  include Mongoid::Attributes::Dynamic
  include Organization::Elasticsearchable
end