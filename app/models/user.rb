class User
  include Documentable
  include GlobalID::Identification
  include Nameable
  include Commentable
  include User::Devisable
  include User::DeviseAsyncable
  include User::Authorizable
  include User::Adminable
  include User::Filterable
  include User::Restoreable
  include User::Gravatarable
  include User::Searchable
  include User::Subscribable
  include User::Companyable
  include User::Sessionable

  field :phone, type: String
  field :time_zone, type: String, default: 'Europe/Moscow'
  field :filters_quantity, type: Integer

  has_many :tenders

  def confirm
    BillingService.change_customer_email email, unconfirmed_email if unconfirmed_email
    super
  end
end
