class Category
  include Documentable
  include TreeViewable

  field :codes, type: Array
end