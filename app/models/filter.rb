class Filter
  include Documentable
  include Companyable
  include Userable
  include Authorizable
  include Filter::Publicable
  include Filter::Searchable
  include Filter::Aggregateable
  include Filter::Restoreable
  include Filter::Copyable
  include Filter::Rebuildable
  include Filter::Quantityable

  field :name, type: String
  field :errors_search, type: Boolean, default: true
  field :any_words, type: String
  field :excluding_errors_search, type: Boolean, default: false
  field :is_analytic, type: Boolean, default: false
  field :ep_type_show, type: Boolean

  # field :share_key, type: String
  # t.datetime "created_at",                                                 :null => false
  # t.datetime "updated_at",                                                 :null => false

  index name: 1

  validates_presence_of :name
end