class Author
  include Documentable
  include Nameable

  field :email, type: String
  field :avatar_image_url, type: String
end