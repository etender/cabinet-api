class Browsing
  include Documentable
  include Tenderable
  include Userable

  index({ tender: 1, user: 1 }, unique: true)
end