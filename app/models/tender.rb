class Tender
  include Documentable
  include Mongoid::Attributes::Dynamic
  include Companyable
  include Authorizable
  include Userable
  include Tender::Commentable
  include Tender::Elasticsearchable
  include Tender::Browseable
  include Tender::Favoritesable
  include Tender::Searchable
  include Tender::Lotable
  include Tender::Guaranteeable
  include Tender::Changeable
  include Tender::Stateable
  include Tender::Restoreable
  include Tender::Guidable
  include Tender::Dateable

  field :origin_id, type: String
  field :base_id, type: String
  field :plan_number, type: String
  field :area, type: String
  field :area_name, type: String
  field :area_url, type: String
  field :auction_area_name, type: String
  field :auction_area_url, type: String
  field :federal_law, type: String
  field :federal_law_name, type: String
  field :region, type: String
  field :url, type: String
  field :name, type: String
  field :price, type: Float
  field :currency, type: String
  field :type, type: String
  field :type_name, type: String
  field :status, type: Symbol
  field :customer, type: Hash
  field :customers, type: Array
  field :organizer, type: Hash
  field :lots, type: Array
  field :documents, type: Array

  attr_accessor :highlight, :documents_highlight

  # Used to update status of complete tenders with Tender::Status::RebuildJob
  index status: 1, end_at: 1
  index({ guid: 1, company_id: 1 }, unique: true)
end