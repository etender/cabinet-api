class Comment
  include Documentable
  include Userable
  include Authorizable
  include Authorable
  include Tenderable
  include Companyable
  include Comment::Searchable

  field :message, type: String
  field :read_only, type: Boolean
end