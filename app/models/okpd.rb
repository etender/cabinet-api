class Okpd
  include Documentable
  include TreeViewable

  def self.normalize(code)
    code[/^[A-Z]\.(.*)$/, 1]
  end
end