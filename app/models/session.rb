class Session
  include Documentable
  include Userable
  include Session::Secretable
  include Session::Lockable
end