class ShortLink
  include Documentable

  field :url, type: String
  field :short_url, type: String
  field :slug, type: String

  before_create :generate_short_url

  index({ slug: 1 }, unique: true)

  scope :outdated, lambda { lte(created_at: Time.now - 30.days) }

  def self.generate(url)
    retries = 0

    begin
      ShortLink.create url: url
    rescue Mongo::Error::OperationFailure => exception
      raise exception unless exception.message.include?('E11000')

      if (retries += 1) <= 5
        retry
      end

      raise exception
    end
  end

  private

  def generate_short_url
    self.slug = SecureRandom.urlsafe_base64 10
    host = url[/((?:http|https):\/\/[^\/]+)/, 1]
    self.short_url = Rails.application.routes.url_helpers.short_link_url(slug, host: host)
  end
end