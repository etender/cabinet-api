class Region
  include Documentable
  include Region::Searchable

  field :name, type: String
  field :title, type: String
  field :code, type: Integer
  field :query, type: String

  index name: 1

  validates_presence_of :name
  validates_presence_of :code
end