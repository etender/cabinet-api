class History
  include Documentable
  include Tenderable
  include Companyable

  field :modifications, type: Hash
end