module Analytics
  module Regions
    class Filter
      include Documentable
      include Analytics::Regions::Filter::Searchable
    end
  end
end