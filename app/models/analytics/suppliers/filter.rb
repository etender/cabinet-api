module Analytics
  module Suppliers
    class Filter
      include Documentable
      include Analytics::Suppliers::Filter::Searchable
    end
  end
end
