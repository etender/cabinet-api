module Analytics
  module Suppliers
    class SupplierDescription
      include Documentable
      include Mongoid::Attributes::Dynamic
      include Analytics::Suppliers::SupplierDescription::Elasticsearchable
    end
  end
end