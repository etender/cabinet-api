module Analytics
  module Suppliers
    class Tender
      include Documentable
      include Analytics::Suppliers::Tender::Elasticsearchable

      field :contracts, type: Hash
      field :participation_count, type: Hash
      field :average_price_drop, type: Hash
      field :customers_count, type: Hash
      field :average_suppliers_count, type: Hash
      field :participation_admission, type: Hash
    end
  end
end