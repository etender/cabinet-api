module Analytics
  module Suppliers
    class Purchase
      include Documentable
      include Analytics::Suppliers::Purchase::Elasticsearchable

      field :tender_name, type: String
      field :tender_url, type: String
      field :published_at, type: Time
      field :supplier_price, type: Float
      field :supplier_inn, type: String
      field :supplier_name, type: String
      field :price_drop, type: Integer
      field :customer_name, type: String
    end
  end
end