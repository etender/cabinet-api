module Analytics
  module Customers
    class Supplier
      include Documentable
      include Analytics::Customers::Supplier::Elasticsearchable

      field :name, type: String
      field :inn, type: String
      field :participation_count, type: Integer
      field :wins_count, type: Integer
      field :price, type: Float
      field :price_drop, type: Integer
    end
  end
end