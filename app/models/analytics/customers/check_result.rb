module Analytics
  module Customers
    class CheckResult
      include Documentable
      include Mongoid::Attributes::Dynamic
      include Analytics::Customers::CheckResult::Elasticsearchable
    end
  end
end