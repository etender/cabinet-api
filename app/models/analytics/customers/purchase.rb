module Analytics
  module Customers
    class Purchase
      include Documentable
      include Analytics::Customers::Purchase::Elasticsearchable

      field :tender_name, type: String
      field :tender_url, type: String
      field :published_at, type: Time
      field :area_name, type: String
      field :area_url, type: String
      field :lot_name, type: String
      field :lot_price, type: Float
      field :supplier_inn, type: String
      field :supplier_name, type: String
      field :suppliers_count, type: Integer
      field :price_drop, type: Integer
    end
  end
end