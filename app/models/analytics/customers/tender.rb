module Analytics
  module Customers
    class Tender
      include Documentable
      include Analytics::Customers::Tender::Elasticsearchable

      field :active_tenders, type: Hash
      field :average_price, type: Hash
      field :average_suppliers_count, type: Hash
      field :average_price_drop, type: Hash
    end
  end
end