module Analytics
  module Customers
    class Filter
      include Documentable
      include Analytics::Customers::Filter::Searchable
    end
  end
end