class RegionSerializer < ActiveModel::MongoidSerializer
  attributes :id, :title

  def id
    object.code
  end
end
