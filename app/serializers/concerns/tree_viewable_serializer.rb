module TreeViewableSerializer
  extend ActiveSupport::Concern

  included do
    attributes :id, :parent_id, :name, :child_ids, :has_child
  end

  def has_child
    object.child_ids.present?
  end
end
