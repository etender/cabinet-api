module TenderableSerializer
  extend ActiveSupport::Concern

  included do
    attributes :base_id, :origin_id, :plan_number, :url, :name, :price, :currency, :type, :type_name,
               :area, :area_name, :auction_area_name, :auction_area_url,
               :federal_law, :federal_law_name, :region, :status, :state, :stage,
               :purchase_graph_execution, :purchase_graph_placing, :purchase_period,
               :published_at, :modified_at, :end_at, :favorites, :browsed,
               :customer, :organizer, :lots_number, :start_at, :auction_at

    belongs_to :user

    attribute(:id) { object.guid }
    attribute(:is_owner) { object.user_id && object.user_id === scope.current_user.id }
    attribute(:recent) { (object.created_at || object.published_at) > Time.now - 3.days && !object.browsed }
    attribute(:time_left) {object.time_left}
  end

  def auction_area_url
    return nil unless object.auction_area_url
    object.auction_area_url[/http|https/] ? object.auction_area_url : "http://#{object.auction_area_url}"
  end
end
