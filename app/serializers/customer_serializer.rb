class CustomerSerializer < ActiveModel::MongoidSerializer
  attributes :id, :name, :fact_address, :post_address, :person, :email, :phone, :fax, :inn, :kpp, :ogrn, :okato
end