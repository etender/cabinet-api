class FilterSerializer < ActiveModel::MongoidSerializer
  attributes :id, :name, :federal_law, :keywords, :deal, :including, :excluding, :pub_date_from, :pub_date_to,
             :end_date_from, :end_date_to, :contact, :price_from, :price_to, :attachment_search, :has_payment, :stage,
             :count, :persisted, :okved

  attribute(:user_id) { object.user_id.to_s }

  has_many :areas
  has_many :forms
  has_many :regions
  has_many :okpds
  has_many :categories

  def regions
    Region.in code: object.regions
  end

  def categories
    Category.in id: object.categories
  end

  def okpds
    Okpd.in id: object.okpds
  end

  def delivery_places
    Region.in code: object.delivery_places
  end

  def persisted
    object.persisted?
  end
end
