module Analytics
  module Regions
    class FilterSerializer < ActiveModel::MongoidSerializer
      attributes :id, :federal_law, :keywords, :including, :excluding,
                 :pub_date_from, :pub_date_to

      has_many :regions
      has_many :okpds

      def regions
        ::Region.in code: object.regions
      end

      def okpds
        Okpd.in id: object.okpds
      end
    end
  end
end