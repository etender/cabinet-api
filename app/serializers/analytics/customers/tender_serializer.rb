module Analytics
  module Customers
    class TenderSerializer < ActiveModel::Serializer
      attributes :active_tenders, :average_price, :average_suppliers_count, :average_price_drop
    end
  end
end