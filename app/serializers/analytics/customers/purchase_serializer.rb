module Analytics
  module Customers
    class PurchaseSerializer < ActiveModel::MongoidSerializer
      attributes :id, :tender_name, :tender_url,
                 :area_name, :area_url,
                 :lot_name, :lot_price, :published_at,
                 :supplier_name, :suppliers_count, :price_drop
    end
  end
end