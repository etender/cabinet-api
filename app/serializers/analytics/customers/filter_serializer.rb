module Analytics
  module Customers
    class FilterSerializer < ActiveModel::MongoidSerializer
      attributes :id, :customer_inn, :customer_name, :customers, :federal_law, :keywords, :including, :excluding,
                 :pub_date_from, :pub_date_to

      has_many :regions
      has_many :okpds
      has_many :categories

      def regions
        ::Region.in code: object.regions
      end

      def categories
        Category.in id: object.categories
      end

      def okpds
        Okpd.in id: object.okpds
      end
    end
  end
end