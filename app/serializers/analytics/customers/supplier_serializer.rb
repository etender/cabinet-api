module Analytics
  module Customers
    class SupplierSerializer < ActiveModel::MongoidSerializer
      attributes :name, :inn, :participation_count, :wins_count, :price, :price_drop
    end
  end
end