module Analytics
  module Customers
    class CheckResultSerializer < ActiveModel::MongoidSerializer
      attributes :type, :count, :violations_recognized, :violations_partially_recognized, :no_violations
    end
  end
end