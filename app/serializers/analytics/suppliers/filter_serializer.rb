module Analytics
  module Suppliers
    class FilterSerializer < ActiveModel::MongoidSerializer
      attributes :id, :federal_law, :keywords, :including, :excluding,
                 :pub_date_from, :pub_date_to, :suppliers

      has_many :okpds

      def okpds
        Okpd.in id: object.okpds
      end
    end
  end
end