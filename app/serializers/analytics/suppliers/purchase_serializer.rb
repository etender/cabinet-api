module Analytics
  module Suppliers
    class PurchaseSerializer < ActiveModel::MongoidSerializer
      attributes :id, :tender_name, :tender_url, :supplier_price, :published_at, :supplier_inn, :supplier_name, :price_drop, :customer_name
    end
  end
end