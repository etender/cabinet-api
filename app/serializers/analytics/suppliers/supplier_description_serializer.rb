module Analytics
  module Suppliers
    class SupplierDescriptionSerializer < ActiveModel::MongoidSerializer
      attributes :name, :inn, :kpp, :ogrn
    end
  end
end