module Analytics
  module Suppliers
    class TenderSerializer < ActiveModel::Serializer
      attributes :contracts, :participation_count, :average_price_drop, :customers_count, :average_suppliers_count, :participation_admission
    end
  end
end