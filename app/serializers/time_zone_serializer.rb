class TimeZoneSerializer < ActiveModel::Serializer
  attribute(:name) { object.tzinfo.name }
  attribute(:title) { object.to_s }
end