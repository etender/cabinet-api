class FilterSummarySerializer < ActiveModel::MongoidSerializer
  attributes :id, :name, :count, :user_id

  attribute(:user_id) { object.user_id.to_s }
end
