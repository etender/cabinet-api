class SuggestionSerializer < ActiveModel::Serializer
  attributes :id, :title
end