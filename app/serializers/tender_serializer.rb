class TenderSerializer < ActiveModel::Serializer
  include TenderableSerializer

  attributes :summingup_at, :examination_at,  :lots, :show_lots, :guarantee_app, :guarantee_contract

  attribute(:duration_at) do
    object.auction_at || object.summingup_at || object.examination_at
  end

  attribute(:documents) do
    object.documents && object.documents.select{ |item| !item.blank? }
  end
end