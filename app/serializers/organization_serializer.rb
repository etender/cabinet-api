class OrganizationSerializer < ActiveModel::Serializer
  attributes :id, :title, :sub_title

  def id
    object.inn
  end
end