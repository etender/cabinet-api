class TenderSummarySerializer < ActiveModel::MongoidSerializer
  include TenderableSerializer

  attributes :highlight

  has_many :documents_highlight, serializer: DocHighlightSerializer
  has_many :has_payments_highlight, serializer: HighlightSerializer
end