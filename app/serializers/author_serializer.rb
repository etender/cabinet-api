class AuthorSerializer < ActiveModel::Serializer
  attributes :name, :surname, :patronymic, :full_name, :avatar_image_url, :created_at
end