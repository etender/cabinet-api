class CommentSerializer < ActiveModel::MongoidSerializer
  attributes :id, :message, :read_only, :created_at

  attribute(:user_id) { object.user_id.to_s }

  has_one :author
end