module Api
  module Private
    class UserSerializer < ActiveModel::MongoidSerializer
      attributes :id, :email

      attribute(:company_name) { object.company.name }
    end
  end
end