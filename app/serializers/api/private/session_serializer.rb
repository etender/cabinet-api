module Api
  module Private
    class SessionSerializer < ActiveModel::Serializer
      attributes :secret
    end
  end
end
