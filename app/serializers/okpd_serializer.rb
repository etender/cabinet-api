class OkpdSerializer < ActiveModel::MongoidSerializer
  include TreeViewableSerializer

  def name
    [Okpd.normalize(object.id), object.name].join' '
  end
end
