class DocHighlightSerializer < ActiveModel::Serializer
  [:attachment_url, :attachment_filename].each { |attr| attribute(attr) { object.dig :_source, attr } }

  attribute(:attachment_content) { object.dig :highlight, 'attachment.content' }
  attribute(:attachment_content_exact) { object.dig :highlight, 'attachment.content.exact' }
end