class ShortLinkSerializer < ActiveModel::Serializer
  attribute(:url) { object.short_url }
end