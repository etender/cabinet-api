class CompanySerializer < ActiveModel::Serializer
  attributes :tariff, :tariff_end_at, :user_limit
end