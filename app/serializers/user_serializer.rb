class UserSerializer < ActiveModel::MongoidSerializer
  attributes :id, :email, :name, :surname, :patronymic, :full_name, :phone, :time_zone, :unconfirmed_email,
             :avatar_image_url, :admin, :coordinator, :foreign, :tenders_change_subscription, :tenders_new_subscription,
             :read_only

  attribute(:can_subscribe) { !object.foreign? || object.email_changed_at.present? }
  attribute(:email_changed) { object.email_changed_at.present? }
  attribute(:invited) do
    object.invitation_sent_at.present? && (
      !object.invitation_accepted? ||
      object.invitation_sent_at > object.invitation_accepted_at
    )
  end

  attribute(:first_entry) { object.last_sign_in_at.blank? }

  attribute(:time_zone_title) { ActiveSupport::TimeZone[object.time_zone].to_s }

  has_one :company, serializer: CompanySerializer
end
