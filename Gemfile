source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.0'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.1'

# Use Puma as the app server
gem 'puma', '~> 3.11'

# Use SCSS for stylesheets
# gem 'sass-rails', '~> 5.0'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.5'

# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 4.0'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Ruby ODM (Object-Document-Mapper) framework for MongoDB
gem 'mongoid', '~> 7.0'

# Rails haml
gem 'haml-rails', '~> 2.0'

# Mailer inline css
gem 'premailer-rails', '~> 1.10'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Authentication
gem 'devise', '~> 4.5'
gem 'devise-i18n', '~> 1.7'
gem 'devise_invitable', '~> 1.7'

# ActiveModel::Serializer implementation and Rails hooks
gem 'active_model_serializers', '~> 0.10'

# Use sidekiq
gem 'sidekiq', '~> 5.2'
gem 'sidekiq_mailer', '~> 0.0'
gem 'sidekiq-scheduler', '~> 3.0'
gem 'sidekiq-limit_fetch', '~> 3.4'

# Use kaminari for paginator.
# The pagination gems must be added before the Elasticsearch gems
gem 'kaminari-mongoid'

# Use elastinsearcg
gem 'elasticsearch'
# Contains search integration for Mongoid
gem 'elasticsearch-model', '~> 6.0'
# Contains various features for Ruby on Rails applications
gem 'elasticsearch-rails', '~> 6.0'

# Support russian I18n locale
gem 'russian', '~> 0.6'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

# User gravatar
gem 'gravatar-ultimate'
gem 'xmlrpc'

# Excel templates
gem 'axlsx_rails'

gem 'rails_same_site_cookie'

gem 'dotiw'

# Rest client
gem 'rest-client', '~> 2.1'

group :development, :test do
  # Rspec
  gem 'rspec-rails', '~> 3.8'

  # A library for setting up Ruby objects as test data
  gem 'factory_bot_rails', '~> 4.11'

  # Get rubocop code validation
  gem 'rubocop', '~> 0.60', require: false

  gem 'rubocop-rspec', '~> 1.37'
end

group :development do
  # Use Capistrano for deployment
  gem 'capistrano-bundler', '~> 1.4'
  gem 'capistrano-maintenance', '~> 1.2', github: 'capistrano/maintenance', require: false
  gem 'capistrano-rails', '~> 1.4'
  gem 'capistrano-rbenv', '~> 2.1'
  gem 'capistrano-rbenv-install', '~> 1.2'
  gem 'capistrano-safe-deploy-to', '~> 1.1', github: 'maxkazar/capistrano-safe-deploy-to'
  gem 'capistrano-logrotate', '~> 0.1', github: 'arcreative/capistrano-logrotate', require: false
  gem 'capistrano-sidekiq', '~> 1.0'
  gem 'capistrano3-puma', '~> 3.1'

  # Disable until merge pull request https://github.com/rweng/pry-rails/pull/104
  gem 'pry-rails', '~> 0.3'

  gem 'listen', '~> 3.1'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring', '~> 2.0'
  gem 'spring-watcher-listen', '~> 2.0'
  gem 'spring-commands-rspec', '~> 1.0'
end
